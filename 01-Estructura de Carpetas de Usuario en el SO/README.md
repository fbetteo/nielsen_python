# Estructura de Carpetas de Usuario en el Sistema Operativo

La estructura del usuario en el sistema operativo es algo fundamental en Python, porque es donde van a arrancar las IDEs para considerar que es el directorio de trabajo, así que si no queremos andar buceando en 800 subcarpetas (e inclusive no poder ir a un nivel superior porque a los desarrolladores les pintó), lo mejor es entender dónde estamos parados y dónde nos conviene laburar.

## Sistemas basados en UNIX

En sistemas basados en Unix (MaxOS, UNIX y Linux) la carpeta de usuario se encuentra en el directorio `/home/<user>`. Seguro los usuarios de Linux lo saben porque no tienen amigos.

## El backstage

En Windows, cuando entramos al Windows Explorer, accedemos a una estructura muy monona, donde tenemos una carpeta de Descargas, una de Imágenes, una de Música, el Escritorio, la de Documentos y la de Videos con el video de muestra que en una época lo protagonizaban 2 actores de Friends.

La posta es que todo se encuentra en el directorio `C:\Users\<user>` y ahí es donde toda la magia ocurre.
