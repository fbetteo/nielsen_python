# Intro a Python

## Qué es?

`Python` es un lenguaje multipropósito creado por Guido van Rossum en 1991 con énfasis es legibilidad del código.

## The Zen of Python

* Beautiful is better than ugly.
* Explicit is better than implicit.
* Simple is better than complex.
* Complex is better than complicated.
* Flat is better than nested.
* Sparse is better than dense.
* Readability counts.
* Special cases aren't special enough to break the rules.
* Although practicality beats purity.
* Errors should never pass silently.
* Unless explicitly silenced.
* In the face of ambiguity, refuse the temptation to guess.
* There should be one-- and preferably only one --obvious way to do it.
* Although that way may not be obvious at first unless you're Dutch.
* Now is better than never.
* Although never is often better than *right* now.
* If the implementation is hard to explain, it's a bad idea.
* If the implementation is easy to explain, it may be a good idea.
* Namespaces are one honking great idea -- let's do more of those!

## Tipado dinámico

Python es un lenguaje de tipado dinámico. Esto quiere decir que el tipo de la variable se va a fijar en el momento de darle valor.

La consecuencia de esto es que para ciertas tareas no sea lo más eficiente.

## Lenguaje indentado

La indentación (o tabulación) en Python no es meramente una buena práctica, en Python es obligatorio para determinar bloques de código.

## () o no ()

Para quien viene de programar en otro lenguaje pensado en datos, Python puede resultar un poco molesto cuando hay que distinguir entre métodos y características.

Python es un leguaje orientado a objetos y cuando quiero obtener algo de una variable, la sintaxis a veces me va a pedir que a lo que pida lo siga () o no.

Con el uso nos vamos a ir dando cuénta de que para métodos se usa () y para atributos no.