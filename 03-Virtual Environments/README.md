# Virtual Environment

A veces las cosas no funcionan exactamente igual de versión en versión de Python.

Es por eso que tal vez con una versión de una librería, pinche un script hecho con otra versión.

Es por eso que están los `virtual environment`, que son instalaciones aisladas de Python (con la versión elegida) con sus respectivas librerías.

Lo bueno es que se pueden exportar las especificaciones y así todas las personas corren sobre lo mismo.

## Instalando desde un archivo

Existen varias formas de crear un `virtual environment`. Para este curso, está [este archivo para Windows](src/environmentWin.yml) y [este archivo para Linux](src/environmentLinux.yml).

Para instalarlo, descargarlo y en la carpeta donde lo bajamos escribir el comando (desde la consola de Anaconda).

```bash
conda env create -f environment<Correspondiente al Sistema operativo>.yml
```

Va a instalar las versiones que necesitamos.

### Agregar nb_conda

Para poder usar el environment desde Jupyter (ya vamos a llegar a las IDEs) tenemos que instalar nb_conda.

Para eso, escribir:

```
pip install nb_conda
```

## Usando el environment

Para poder usar el environment, tenemos que escribir en la consola.

```
conda activate <nombre del environment>
```

Para salir:

```
conda deactivate
```

## Documentación

Para saber más sobre los environments: [https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)