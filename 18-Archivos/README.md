# Archivos

## os

La librería `os` contiene funciones que permiten trabajar con las carpetas del sistema operativo.

### getcwd

La función `getcwd` nos va a permitir saber cuál es la carpeta en la que estamos trabajando.

```python
import os
os.getcwd()
```

### path.join

La función `path.join` devuelve un string uniendo los argumentos que le pasemos por el separador de carpetas del sistema operativo. En caso de Windows `\` en caso de sistemas basados en UNIX `/`.

```python
os.path.join('.','16-Archivos','user')
```

### listdir

La función `listdir` nos va a devolver una lista con los nombres de las carpetas y archivos de nuestro directorio de trabajo.

```python
os.listdir()
```

### chdir

La función `chdir` va a cambiar el directorio de trabajo a la que le indiquemos.

```python
os.chdir(os.path.join('18-Archivos','data'))
```

## open

La función `open` srive para abrir una conexión con un archivo.

Posee 2 argumentos: nombre del archivo y tipo de conexión (lectura/escritura y binario/texto).

```python
f = open('Vb_semanalizadas.csv','rt')
```

>Le indicamos que es conexión de `lectura` en el modo `binario`

Lo que va a crear un un objeto de tipo ` _io.TextIOWrapper` que va a tener una conexión con el archivo que abrimos.

## read

El método `read` nos va a devolver un string con el texto del archivo.

El argumento opcional que podemos darle es la cantidad de caracteres a leer.

```python
texto = f.read()
texto
```

Una vez que utilizamos el método read, el objeto ya no poseerá texto. Por eso, debemos cerrar la conexión con el método `close`.

```python
f.close()
```

### with

La sentencia `with` permite abrir una conexión, realizar operaciones y una vez que se termina de ejecutar el bloque de código, se cierra la conexión.

```python
with open('Vb_semanalizadas.csv','rt') as f:
    texto = f.read()
```

## readlines

El método `readlines` va a devolver una lista que contendrá en cada elemento una línea del texto del archivo.

```python
with open('Vb_semanalizadas.csv','rt') as f:
    texto_lista = f.readlines()

texto_lista[:10]
```

## write

Para escribir un archivo tengo que abrirlo en modo escritura

```python
with open(os.path.join('..','user','miArchivo.txt'),'wt') as f:
    f.write('Holiiiis')
```

Si el archivo existe, abriendo la conexión con el modo `w` lo va a sobreescribir.

```python
with open(os.path.join('..','user','miArchivo.txt'),'wt') as f:
    f.write('Epa!!!')
```

Si queremos agregar texto, tenemos que abrirlo en modo `a`  de append.

```python
with open(os.path.join('..','user','miArchivo.txt'),'at') as f:
    f.write('Otra vez!!!')
```

En el ejemplo anterior se agrega en la misma línea inmediatamente después del texto existente. Si quisiéramos agregar en la línea siguiente, deberíamos agregar una nueva línea en nuestro texto (\n)

```python
with open(os.path.join('..','user','miArchivo.txt'),'at') as f:
    f.write('\nNueva línea')
```

## Pickle

`pickle` es una librería que permite guardar los objetos de Python de modo binario.

### dump

Para guardar objetos, se debe abrir una conexión de escritura (w) en modo binario (b) a un archivo y utilizar la función `dump`.

```python
import pickle as pkl
with open(os.path.join('..','user','pickle.pkl'),'wb') as f:
    pkl.dump(
        {'a':[1,2,3,4],
         'b':'Si vos querés...'},
         f
    )
```

### load

Para importar objetos exportados, tenemos que abrir una conexión de lectura en modo binario y usar la función `load` asignando a una variable lo que estemos importando.

```python
with open(os.path.join('..','user','pickle.pkl'),'rb') as f:
    importacion = pkl.load(f)
```

## Ejercicio

Importar el archivo `Vb_semanalizadas.csv` de la carpeta `data` y calcular la suma de value por variable, por mes, por año y por fecha.

Calcular el promedio de ValueSplit por variable-Mes-año.

Son libres de resolverlo como quieren, para eso, les recomiendo usar diccionarios.

Para hacer una suma acumulativa hay el operador `+=` suma una cantidad a una variable.

```python
a=1
a+=3
print(a)
```
