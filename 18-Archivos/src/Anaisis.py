#%% [Markdown]
# Importo Datos 
#%%
import os
os.getcwd()

#%%
os.chdir(os.path.join('18-Archivos','data'))

#%%
with open('Vb_semanalizadas.csv','rt') as f:
    data = f.readlines()

print(data[:10])

#%% [markdown]
# Elimino el salto de línea de cada línea

#%%
data = [l[:-1] for l in data]
data[:10]

#%% [markdown]
# Separo entre encabezado y datos

#%%
encabezado = data[0]
datos = data[1:]

#%% [markdown]
# Separo por `,`

#%%
encabezado = encabezado.split(',')
datos = [l.split(',') for l in datos]
datos[:10]
#%% [markdown]
# Mapeo cada dato a su variable

#%%
def mapeo(encabezado,valores):
    return {k:v for k,v in zip(encabezado,valores)}

datos_mapeados = [mapeo(encabezado,l) for l in datos]
datos_mapeados[:3]
#%% [markdown]
# Cambio el tipo de dato de value u ValueSplit

#%%
for dic in datos_mapeados:
    dic['value']=float(dic['value'])
    dic['ValueSplit']=float(dic['ValueSplit'])

#%% [markdown]
# Creo una función que cuando le pase una clave del diccionario, me sume el campo por valor de la clave.


#%%
def sumoPorGrupo(clave):
    #Vamos a crear Un diccionario que sea: Variable por la que queremos agrupar: suma de value
    dic = {}
    #Itero cada fila
    for row in datos_mapeados:
        #Si Ya tengo un registro en el diccionario de sumas, lo sumo
        if row[clave] in dic.keys():
            dic[row[clave]]+=row['value']
        #Si Ya tengo un registro en el diccionario de sumas, lo sumo creo
        else:
            dic[row[clave]]=row['value']
    return dic

#%%
sumoPorGrupo('variable')

#%%
sumoPorGrupo('month')

#%%
sumoPorGrupo('year')

#%%
sumoPorGrupo('Date')

#%% [markdown]
# Calculo el promedio por variable-mes-año
#%%
#Creo el diccionario
#Comienzo generando un diccionario vacío, cuya clave sea la combinación de variable, year y month y su valor sea una tupla (suma de ValueSplit,cantidad)
dic = {}
for row in datos_mapeados:
    if "{} {} {}".format(row['variable'],row['year'],row['month']) in row.keys():
        dic["{} {} {}".format(row['variable'],row['year'],row['month'])][0]+=row['ValueSplit']
        dic["{} {} {}".format(row['variable'],row['year'],row['month'])][1]+=1
    else:
        dic["{} {} {}".format(row['variable'],row['year'],row['month'])]=(row['ValueSplit'],1)
{k:v[0]/v[1] for k,v in dic.items()}
        

#%%
