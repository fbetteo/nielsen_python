#%% [markdown]
# # Paramiko
#
# Importamos la librería

#%%
import paramiko

#%% [markdown]
# Generamos un objeto del tipo `SSHClient`

#%%
client = paramiko.SSHClient()

#%% [markdown]
# Indicamos que si no tengo la key de ssh, que la agregue automáticamente

#%%
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#%% [markdown]
# Creamos la conexión

#%%
host = 
user =
password = 
client.connect(host, username=user, password=password)

#%% [markdown]
# Ejecutamos un comando

#%%
stdin, stdout, stderr = client.exec_command(ls)

#%% [markdown]
# Nos genera 3 objetos:
#
# * Entrada
# * Salida
# * Error
#
# Para poder ver el contenido, nos conviene hacer un `readlines`

#%%
archivos = stdout.readlines()
print(archivos)

#%% [markdown]
# Eliminamos el salto de línea del texto

#%%
archivos=[x[:-1] for x in archivos]

#%% [markdown]
# Otro comando

#%%
stdin, stdout, stderr = client.exec_command('pwd')
ubicacion=stdout.readlines()
print(ubicacion)

#%% [markdown]
# ## sftp
#
# Para transferir archivos debemos empezar creando un objeto de tipo `Transport`

#%%
host =
user =
password =
port = 22 
transport = paramiko.Transport((host,port))

#%% [markdown]
# Nos conectamos.

#%%
transport.connect(None,user,password)

#%% [markdown]
# Creamos un objeto de tipo `SFTPClient`.

#%%
sftp = paramiko.SFTPClient.from_transport(transport)

#%% [markdown]
# Con la función `get` bajamos archivos

#%%
origin_path = 
destination_path=
sftp.get(origin_path,destination_path)

#%% [markdown]
# Con la función `put` subimos archivos

#%%
origin_path = 
destination_path=
sftp.put(origin_path,destination_path)