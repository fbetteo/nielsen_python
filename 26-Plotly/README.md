# Plotly

`Plotly` es una librería gráfica que permite crear gráficos interactivos.

## Figure

Existen varias formas de crear gráficos en Plotly, una de ellas es usando objetos del tipo `Figure` que pertenecen al módulo `graph_objects`.

```python
import plotly.graph_objects as go
fig=go.Figure()
```
## add_trace

Para agregar elementos sobre nuestra figura debemos utilizar el método `add_trace`.

Cada gráfico va a tener su función de `graphic_objects`.

```python
fig.add_trace(
    go.Bar(x=[1, 2, 3], y=[1, 3, 2])
    )
```

## show

La función `show` muestra el gráfico generado.

```python
fig.show()
```

## Scatter

La función `Scatter` sirve tanto para gráficos de línea como scatterplots.

Los parámetros más usuales son:

* x
* y
* mode: El tipo de gráfico.
    * 'markers': Scatterplot o bubble
    * 'lines': Líneas
    * 'lines+markers': Líneas con marcador en los puntos.
* name: El nombre con el que se va a identificar en la leyenda.
* marker: Un diccionario con las opciones del marcador como tamaño y color.

```python
import plotly.express.data as data
iris=data.iris()
fig=go.Figure()
fig.add_trace(
    go.Scatter(x=iris.loc[iris.species=='setosa','sepal_length'],
               y=iris.loc[iris.species=='setosa','sepal_width'],
               mode='markers',
               marker={"size":iris.loc[iris.species=='setosa','petal_width']*20,
                       "color":"red"},
               name="Setosa"
              )
)

fig.add_trace(
    go.Scatter(x=iris.loc[iris.species=='versicolor','sepal_length'],
               y=iris.loc[iris.species=='versicolor','sepal_width'],
               mode='markers',
               marker={"size":iris.loc[iris.species=='versicolor','petal_width']*20,
                       "color":"green"},
               name="Versicolor"
              )
)

fig.add_trace(
    go.Scatter(x=iris.loc[iris.species=='virginica','sepal_length'],
               y=iris.loc[iris.species=='virginica','sepal_width'],
               mode='markers',
               marker={"size":iris.loc[iris.species=='virginica','petal_width']*20,
                       "color":"blue"},
               name="Virginica"
              )
)
fig.show()
```

## Bar

La función `Bar` crea un gráfico de barras.

```python
animals=['giraffes', 'orangutans', 'monkeys']

fig = go.Figure()

fig.add_trace(go.Bar(name='SF Zoo', x=animals, y=[20, 14, 23]))
fig.add_trace(go.Bar(name='LA Zoo', x=animals, y=[12, 18, 29]))

fig.show()
```

Por defecto hace barras agrupadas.

Si quisiéramos hacer barras _stacked_ tenemos que agregar `fig.update_layout(barmode='stack')`.

```python
animals=['giraffes', 'orangutans', 'monkeys']

fig = go.Figure()

fig.add_trace(go.Bar(name='SF Zoo', x=animals, y=[20, 14, 23]))
fig.add_trace(go.Bar(name='LA Zoo', x=animals, y=[12, 18, 29]))

fig.update_layout(barmode='stack')

fig.show()
```

## Box

La función `Box` crea un boxplot.

```python
fig = go.Figure()

fig.add_trace(go.Box(x=iris.species,y=iris.sepal_length))

fig.show()
```

## Histogram

La función 


```python
import numpy as np
fig=go.Figure()
np.random.seed(42)

x = np.random.randn(500)

fig.add_trace(go.Histogram(x=x))
fig.show()
```

## subplots

Para hacer subplots necesitamos la función `make_subplots` de `plotly.suplots`.

```python
from plotly.subplots import make_subplots
fig = make_subplots(rows=1, cols=2)

fig.add_trace(
    go.Scatter(x=[1, 2, 3], y=[4, 5, 6]),
    row=1, col=1
)

fig.add_trace(
    go.Scatter(x=[20, 30, 40], y=[50, 60, 70]),
    row=1, col=2
)

fig.show()
```

## update_layout

El método `update_layout` permite modificar elementos del gráfico como títulos.

```python
gapminder = data.gapminder()

fig = go.Figure()

for country in [c for c in gapminder.country.unique() if c[0]=='A']:
    fig.add_trace(
        go.Scatter(x=gapminder.loc[gapminder.country==country,'year'],
                   y=gapminder.loc[gapminder.country==country,'gdpPercap'],
                   mode='lines',
                   name=country
        )
    )
fig.update_layout(
    title='PBI per capita de los países que empiezan con "A"',
    xaxis={'title':'Año',
           'tickangle':-45
          },
    yaxis={'title':'PBI'}
)
fig.show()
```