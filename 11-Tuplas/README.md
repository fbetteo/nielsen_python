# Tuplas

Las tuplas son como listas pero inmutables, es decir, no se pueden modificar.

## Creación

Para crear una tupla, se escriben los elementos entre `()` separados por `,`.

```python
tupla = (1,2,3,4)
```
## list

Para convertir una tupla a una lista, se lo hace con la función list.

```python
lista=list(tupla)
```

## Variable Unpacking

Puedo asignar cada valor de una tupla a una variable poniendo los nombres de las variables a la izquierda del `=` separados por `,`.

```python
uno,dos,tres,cuatro=tupla
```