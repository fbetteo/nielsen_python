# WebScrapping

## Estructura de una página (vuelo de pájaro)

Las páginas web son archivos de texto plano con `tags` que el navegador se encarga de mostrarnos de acuerdo al formato que se le otorguen.

```html
<!DOCTYPE html>
<html>
<head>
	<title>Página de Ejemplo</title>
</head>
<body>
<div id='ArribaTodo'>
	<div class='adentro' id='primero'>En este div va a haber <span>Cosas</span></div>
	<div class='adentro' id='segundo'>En este div va a haber <span>Otras Cosas</span></div>
</div>
</body>
</html>
```

Cada tag puede tener diferentes propiedades, entre las más comunes están:

* class: Puede identificar a uno o más tags.
* id: Identifica a un solo tag.

A su vez, los tags están en cascada, es decir, se puede identificar a un tag dentro de otro tag.

## Selenium

Selenium es un sistema que automatiza navegadores.

Para instalarlo, en nuestro environment tenemos que escribir.

```bash
pip install selenium
```

A su vez, para poder usarlo vamos a necesitar el `webdriver`, el cual se descarga de la [página de Selenium](https://www.seleniumhq.org/download/).

Para el del Chrome particularmente, ir a [https://sites.google.com/a/chromium.org/chromedriver/](https://sites.google.com/a/chromium.org/chromedriver/)

Tenemos que guardar el webdriver en alguna ruta que podamos acceder.

