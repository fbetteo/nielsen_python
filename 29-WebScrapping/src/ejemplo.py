#%%
# Importamos el objeto webdriver de Selenium y otras librerías que necesitemos
from selenium import webdriver
import time
import pandas as pd
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#%%
# Agregamos como opción la ruta de descarga y que por defecto no permita notificaciones
download_directory = '/home/pogo/Downloads'
options = webdriver.ChromeOptions()
prefs = {'download.default_directory':download_directory,
         "profile.default_content_setting_values.notifications" : 2}
options.add_experimental_option('prefs',prefs)

#%%
# Inicializamos el driver
ruta_webdriver = '/home/pogo/webdriver/chromedriver'
driver = webdriver.Chrome(ruta_webdriver,options=options)

#%%
# Le decimos que vaya a un sitio
driver.get('https://lucpogo.github.io/prueba_selenium/')

#%%
# Buscamos el elemento cuyo ir es "archivos"
lista_links = driver.find_element_by_id('archivos')

#%%
# Traemos cada ítem de la lista
links = lista_links.find_elements_by_tag_name('li')

#%%
# Iteramos y descargamos
for link in links:
    link=link.find_element_by_tag_name('a')
    link.click()

#%%
# Cerramos el navegador
driver.close()

#%%
# Inicializamos el driver
driver = webdriver.Chrome(ruta_webdriver,options=options)

#%%
# Vamos a Feisbuc
driver.get('http://www.facebook.com')
#%%
# Buscamos el input del user y lo escibimos en el navegador
email = ''
try:
    user = driver.find_element_by_id('email')
except:
    user = driver.find_element_by_name('email')
user.send_keys(email)

#%%
# Levantamos el archivo con nuestro password y lo pasamos al input de password
password_path = ''
with open(password_path) as f:
    password_text=f.read()
try:
    password = driver.find_element_by_id('pass')
except:
    password = driver.find_element_by_name('pass')
password.send_keys(password_text)

#%%
# Clickeamos el botón de login
#%%
try:
    loggin_button = driver.find_element_by_id('loginbutton')
except:
    loggin_button = driver.find_element_by_name('login')
loggin_button.click()

#%%
# Buscamos el link a nuestro perfil
perfil = driver.find_element_by_xpath("//*[@data-click='profile_icon']")
perfil.click()

#%%
# Buscamos la lista de amigos
WebDriverWait(driver, 60).until(
        EC.presence_of_element_located((By.XPATH, "//*[@data-tab-key='friends']"))
    )
amigos = driver.find_element_by_xpath("//*[@data-tab-key='friends']")
amigos.click()

#%%
# Necesitamos scrollear para cargar toda la lista
def scroll(tiempo):
    SCROLL_PAUSE_TIME = tiempo

    last_height = driver.execute_script("return document.body.scrollHeight")
    
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        time.sleep(SCROLL_PAUSE_TIME)
    
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

scroll(1.5)

#%%
# Traemos la lista de amigos
lista_amigos = driver.find_element_by_id('pagelet_timeline_medley_friends')
lista_amigos = lista_amigos.find_elements_by_xpath("//*[@class='fsl fwb fcb']")

#%%
# Nos quedamos con los links
amigos_links = [x.find_element_by_tag_name('a') for x in lista_amigos]

#%%
# Nos quedamos con el nombre y la direccion de los links
amigos_nombres = [x.text for x in amigos_links]
amigos_links = [x.get_attribute('href') for x in amigos_links]

#%%
amigos_df = pd.DataFrame({'amigo':amigos_nombres,'link':amigos_links})

#%%
driver.close()

