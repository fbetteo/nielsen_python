# Expresiones Regulares

![Expresiones Regulares](https://imgs.xkcd.com/comics/regular_expressions.png)

## Definición

Las expresiones regulares son formas de buscar texto usando `pattern matching`.

## Recursos

### Testear expresiones regulares

Existen varios sitios en los cuales uno puede testear expresiones regulares para ver con qué estarían matcheando y con qué no.

Uno muy bueno es : [https://regex101.com/](https://regex101.com/)

### Librería

La librería de python para expresiones regulares es `re`.

## Uso

### Búsqueda

Para devolver texto, lo que vamos a hacer es usar la función `re.findall(expresión regular, texto a buscar)` que nos va a devolver una lista con los textos que coinciden con la expresión regular.

### Tokens

Los tokens son parámetros de búsqueda dentro del texto.

#### Literal

El más simple es una cadena para buscar literalmente algo.

```python
taxi = """que es lo que hace un taxista seduciendo a la vida
que es lo que hace un taxista construyendo una herida
que es lo que hace un taxista enfrente de una dama
que es lo que hace un taxista con sus sueños de cama"""

re.findall('que',taxi)
```

#### ^

El caracter `^` indica que es al comienzo del texto.

```python
re.findall('^que',taxi)
```

#### $

El caracter `$` indica que es al final del texto

```python
re.findall('que$',taxi)
```

#### []

Encerrado entre `[]` vamos a poner un patrón de búsqueda que tenga varias coincidencias.

```python
re.findall('[a-z]','Esto es un teléfono:03-03-456')
```

Como vemos, va a traer todas las letras minúsculas.

```python
re.findall('[a-zA-z]','Esto es un teléfono:03-03-456')
```

```python
re.findall('[0-9]','Esto es un teléfono:03-03-456')
```

#### Repeticiones

##### {}

Encerrado entre `{}` vamos a indicar cuántas veces queremos que se repita cierto patrón.

```python
re.findall('abc{3}','ab ac bc abc abcc abccc abcccc abcccccc')
```

Podemos indicar que se repita en un rango

```python
re.findall('abc{2,3}','ab ac bc abc abcc abccc abcccc abccccc')
```

##### *

El caracter `*` indica que el patrón puede repetirse 0 o más veces

```python
re.findall('[a-zA-Z]*','ab ac bc abc abcc abccc abcccc abccccc')
```

##### +

El caracter `+` indica que el patrón tiene que repetirse al menos 1 vez

```python
re.findall('[0-9]+','Esto es un teléfono:03-03-456')
```

#### Repasemos

```python
contacto='Llamar de 15 a 21 a partir del 01-Agosto-2015 al 011-1234-9876 o al 0223-222-7680 o escribir a mail1@hbhbhb.com o a hbhbhb@kmkmkm.com. Número de CUIT 20-21345678-8 Ref: 0900--99898'

re.findall('[0-9]+-[0-9]+-[0-9]{4}',contacto)
```

#### \

Si queremos buscar un caracter que coincide con un token, debemos anteponer el signo `\`.

```python
re.findall('\[[0-9]*\]','[8787878] o 9898989')
```

#### .

El caracter . nos devuelve cualquier caracter

```python
re.findall('\[.*\]','[8787878], [jbjnjnjnjnnj.---] 9898989')
```


#### Porción

Entre `()` vamos a indicar qué parte queremos quedarnos de lo que coincida.

```python
mails = """arianamacarena.scasserra@nielsen.com
melina.terreno@nielsen.com
micaela.ahedo@nielsen.com
micaela.gimenez@nielsen.com
tomas.pallotti@nielsen.com
franco.x.betteo@nielsen.com
agustina.alzola@nielsen.com
juan.tobares@nielsen.com
ezequiel.prada@nielsen.com
y agregamos huevadas como mails no válidos jnjnjn@jnjnjn.
@twitter
@twitter.com
"""

print(re.findall('[A-Za-z0-9_\.]+@[A-Za-z0-9_]+\.[A-Za-z0-9_]+',mails))

re.findall('([A-Za-z0-9_\.]+)@[A-Za-z0-9_]+\.[A-Za-z0-9_]+',mails)
```

### Sustitución

La función `re.sub(expresión,texto de reemplazo,texto en el que buscar)` hace reemplazos de Texto

```python
re.sub(' +',' ','reemplazo   exceso de    espacios  uiii')
```