# Comentarios

## \#

Para escribir un comentario en Python, se antepone el caracter `#` al comentario y el intérprete va a ignorar todo lo que siga hasta el salto de línea.

```python
#Acá pongo lo que quiera total no voy a romper nada. Viva Perón!
```

## Strings multilínea

Si bien no son comentarios per se, los strings multilínea (encerrados entre `"""`) flotando en un script no son instrucciones, por lo cual se puede poner un "comentario" más largo de ese modo.

```python
"""
Es usual poner comentarios largos en un string multilínea,

Por ejemplo datos acerca de quién hizo el script.

Alguna instrucción de cómo usar una función.

Algún chiste como para quedar en la posteridad, como:
— ¿Por qué el mar no se seca?
— Porque no tiene toalla.

También un ASCII como cuando uno bajaba un crack antes de la masificación del Open Source

      _ _,---._
   ,-','       `-.___
  /-;'               `._
 /\/          ._   _,'o \
( /\       _,--'\,','"`. )
 |\      ,'o     \'    //\
 |      \        /   ,--'""`-.
 :       \_    _/ ,-'         `-._
  \        `--'  /                )
   `.  \`._    ,'     ________,','
     .--`     ,'  ,--` __\___,;'
      \`.,-- ,' ,`_)--'  /`.,'
       \( ;  | | )      (`-/
         `--'| |)       |-/
           | | |        | |
           | | |,.,-.   | |_
           | `./ /   )---`  )
          _|  /    ,',   ,-'
 -hrr-   ,'|_(    /-<._,' |--,
         |    `--'---.     \/ \
         |          / \    /\  \
       ,-^---._     |  \  /  \  \
    ,-'        \----'   \/    \--`.
   /            \              \   \
"""
```
