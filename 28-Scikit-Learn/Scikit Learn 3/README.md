# Clustering

El objetivo de las técnicas de clustering es formar grupos tales que las observaciones que componen cada una estén lo más cerca entre sí y lo más distante con respecto a los otros grupos como sea posible.

## Distancia euclídea

Para evaluar la distancia entre 2 observaciones, se debe definir una medida.

La distancia euclídea se define como:

![Distancia Euclidea](img/euclidea.gif)

### Estandarización

Cuando una dimensión posee mayores distancias entre sus valores, ésta va a tener más peso que las otras.

Es por ello que una práctica usual es la de estandarizar las variables con el fin de poseer magnitudes comparables.

# KMeans

El algoritmo de KMeans procura generar `K` grupos en torno a centroides.

![kmeans](img/kmeans.png)

Un centroide es un punto representado por los promedios de todas las dimensiones de los puntos que componen el cluster.

## Algoritmo

1. Ubicar K puntos en el espacio.
2. Calcular la distancia de cada punto a los K puntos.
3. Asignar cada punto al centroide más cercano.
4. Calcular la media de toda variable para cada cluster.
5. Reasignar el centroide.
6. Ejecutar desde (2) hasta que los centroides se mantengan, se llegue a una diferencia menor a un umbral o bien se alcancen las iteraciones máximas.

## Clase

El módulo donde se encuentra es `sklearn.cluster` y la clase es `KMeans`.

## Documentación

La referencia de la api se encuentra en [https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) y la guía de usuario con tutorial y explicación del modelo en [https://scikit-learn.org/stable/modules/clustering.html#k-means](https://scikit-learn.org/stable/modules/clustering.html#k-means).


## Parámetros

Algunos parámetros que permite modificar:

### n_clusters

Cantidad de clusters. Por defecto es 8.

### max_iter

Cantidad máxima de iteraciones. Por defecto es 300.

# Random Forest

Random Forest se basa en entrenar varios árboles de decisión tomando muestras con reposición de las observaciones y muestras de las variables.

Esto produce que los árboles sean muy diferentes entre sí.

## Algoritmo

1. Seleccionar una cantidad _M_ de árboles a entrenar.
2. En cada iteración:
    1. Tomar una muestra con reposición de las observaciones.
    2. Tomar una muestra de las variables.
    3. Entrenar un árbol.
3. Ensamblar utilizando promedio de probabilidades.

## Clase

El módulo donde se encuentra es `sklearn.ensemble` y la clase es `RandomForestClassifier`.

## Documentación

La referencia de la api se encuentra en [http://scikit-learn.org/0.19/modules/generated/sklearn.ensemble.RandomForestClassifier.html](http://scikit-learn.org/0.19/modules/generated/sklearn.ensemble.RandomForestClassifier.html) y la guía de usuario con tutorial y explicación del modelo en [http://scikit-learn.org/0.19/modules/ensemble.html#forest](http://scikit-learn.org/0.19/modules/ensemble.html#forest).

## Parámetros

Algunos parámetros que permite modificar:

### n_estimators

Cantidad de árboles.

Por defecto son _10_.

### criterion

El criterio para medir la impureza del nodo.

Las opciones son `"gini"` o `"entropy"`.

La opción por defecto es _"gini"_

### max_features

La cantidad de variables a utilizar en cada árbol.

Puede ser _int_, _float_, _string_ o None.

* _int_: Cantidad.
* _float_: Proporción de la cantidad de variables.
* _"auto"_: Raíz cuadrada de la cantidad de variables.
* _"sqrt"_: Raíz cuadrada de la cantidad de variables.
* _"log2"_: Logaritmo en base 2 de la cantidad de variables.
* _None_: Todas las variables.

El valor por defecto es _"auto"_.

### max_depth

La profundidad máxima del árbol.

Tener en cuenta que cuanto más profundo más se va a ajustar a mis datos, pero corro mayor riesgo de overfitting.

Puede ser un número o `None`. En este último caso, continuará hasta encontrar el nodo que ya no abra.

La opción por defecto es _None_.

### min_samples_split

La cantidad mínima de observaciones que tiene que poseer un nodo para intentar una partición.

Tener en cuenta que cuanto menor sea, más se va a ajustar a mis datos, pero corro mayor riesgo de overfitting.

Si es un `número entero`, es la cantidad que tiene que tener, si es un `número entre 0 y 1`, es la proporción de las observaciones.

La opción por defecto es _2_.

### min_samples_leaf

La cantidad mínima de observaciones que tiene que tener cada nodo.

Tener en cuenta que cuanto menor sea, más se va a ajustar a mis datos, pero corro mayor riesgo de overfitting.

Si es un `número entero`, es la cantidad que tiene que tener, si es un `número entre 0 y 1`, es la proporción de las observaciones.

La opción por defecto es _1_.

### min_impurity_decrease

El mínimo cambio en la impureza para partir el nodo.

La opción por defecto es _0_.

### random_state

La semilla aleatoria.

### warm_start

Es un boolean que indica que en caso de volver a ejecutar _.fit_, que reutilice lo entrenado hasta el momento.

Por defecto es _False_.

# Evaluación de modelos

## Matriz de Confusión

La matriz de confusión es una herramienta que nos permite evaluar qué tan bien estamos clasificando.

Consiste en una tabla de doble entrada en la que en las filas representamos el valor real, en las columnas el valor predicho y en las celdas la cantidad de observaciones que corresponde a cada caso.

|          |      |   __PREDICHO__                 |                    |
|----------|----------|--------------------|--------------------|
|          |          | _POSITIVO_           | _NEGATIVO_           |
| __REAL__ | _POSITIVO_ | VERDADERO POSITIVO | FALSO NEGATIVO     |
|          | _NEGATIVO_ | FALSO POSITIVO     | VERDADERO NEGATIVO |

Para poder tener un valor predicho, es necesario establecer un umbral de probabilidad.

### Métricas basadas en matriz de confusión

Teniendo en cuenta un problema de clasificación binario, existen diversas métricas.

#### Accuracy

`(VP+VN)/TOTAL`

Qué porcentaje de los casos fue clasificado correctamente.

Cuando las clases están muy desbalanceadas no sirve.

#### Precisión

`VP/(VP+FP)`

VP/ACEPTADOS

Qué proporción de los valores aceptados son positivos.

#### Sensibilidad / Recall

`VP/(VP+FN)`

VP/POSITIVOS

Qué porcentaje de los positivos son aceptados.

#### Especificidad

`VN/(FP+VN)`

VN/NEGATIVOS

Qué porcentaje de los negativos son rechazados.

#### F1-Score

`2*(precisión+recall)/(precisión*recall)`

Es la media armónica entre Precisión y Sensibilidad. Tener en cuenta que ambas medidas son contrapuestas.

### Cuál Elegir?

Estas métricas están influenciadas por el umbral de probabilidad seleccionado y generalmente son opuestas.

Mayor exigencia => Aumento de umbral => Mayor cantidad de casos rechazados => Aumenta VN y Disminuye VP

Por lo tanto, al elegir una, hay que tener en cuenta el problema:

Si no se quiere dejar a ningún positivo afuera, cuál se debe usar?

`Sensibilidad (VP/POSITIVOS)`

En este caso, algunos exámenes médicos privilegian un falso positivo a que alguien posea una enfermedad y no lo sepa.

Si se quiere estar seguro de no dar por positivo algo falso?

`Especificidad (VN/NEGATIVO)`

### Implementación en Python

En [https://scikit-learn.org/stable/modules/classes.html#module-sklearn.metrics](https://scikit-learn.org/stable/modules/classes.html#module-sklearn.metrics) se pueden observar las diversas métricas.

La documentación se encuentra en [https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics](https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics)
## ROC

La curva ROC sirve para evaluar modelos binarios.

![ROC.png](img/ROC.png)

1. Ordenar las observaciones de mayor a menor valor de probabilidad.
2. Comenzar con el valor más alto de probabilidad y calcular:
    * _porcentaje de casos **positivos** que poseen un valor de probabilidad mayor a ese umbral_ (Sensibilidad o True Positive Rate) 
    * _pocentaje de casos **negativos** que poseen un valor de probabilidad mayor a ese umbral_ (1-Especificidad o False Positive Rate)
3. Representar esos valores en un espacio cartesiano.
4. Hacer lo mismo con el siguiente valor de probabilidad.

Esta curva permite ver qué tan bien está ordenando nuestro modelo.

Cuanto más se aleje la curva de la recta de 45°, mejor es el modelo.

Lo bueno que posee esta forma de evaluar el modelo, es que podemos comparar modelos en diferentes niveles de probabilidad.

<iframe src="https://lucpogo.github.io/ROC/" height=500 width=500>

### Clase

El módulo donde se encuentra es `sklearn.metrics` y la clase es `roc_curve`.

### Documentación

La referencia de la api se encuentra en [https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_curve.html](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_curve.html) y la guía de usuario con tutorial y explicación del modelo en [https://scikit-learn.org/stable/modules/model_evaluation.html#roc-metrics](https://scikit-learn.org/stable/modules/model_evaluation.html#roc-metrics).


### Área

Al área debajo de la curva ROC se la llama AUC y sirve para comparar modelos.

Su interpretación estadística es:

![AUCestadistica.gif](img/AUCestadistica.gif)

Es decir, cuál es la probabilidad de que tomando 1 caso positivo al azar y 1 caso negativo al azar, la probabilidad estimada del caso positivo sea mayor a la del caso negativo.

### Clase

El módulo donde se encuentra es `sklearn.metrics` y la clase es `roc_auc_score`.

