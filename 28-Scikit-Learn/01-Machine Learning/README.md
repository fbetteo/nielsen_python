# Machine Learning

## Qué es Machine Learning?

Para entender qué es `Machine Learning`, empecemos por verlo en acción. Entrá con el celular a  [http://amzn.to/takeselfie](http://amzn.to/takeselfie).

En un caso dio:

> __Person Details:__
> <br>Age Range: 29 - 45
> <br>Beard: true
> <br>Eyeglasses: false
> <br>Gender: Male
> <br>Smiles: false

> __Items found in photo:__
> <br>Face
> <br>Human
> <br>Person
> <br>Beard
> <br>Head
> <br>Portrait
> <br>Photography
> <br>Photo
> <br>Mustache

Imagínense que hubiese que crear un programa utilizando programación tradicional para reconocer una cara o un elemento. Habría que estar programando cada una de las reglas para reconocer un objeto.

Si usáramos `Machine Learning` para resolver el mismo problema (que es lo que usa esta aplicación), lo que tendríamos es un montón de fotos "etiquetadas" (más adelante vamos a entrar en detalle) y un modelo que va a aprender de estos datos.

### Definición

`Machine Learning` es una es una rama de la Inteligencia Artificial
que utiliza técnicas estadísticas que le permiten a la
computadora "aprender" de los datos sin necesidad de
ser programadas.

Qué quiere decir __aprender de los datos__?

En la programación tradicional, tenemos el siguiente esquema:

![programacionTradicional.png](img/programacionTradicional.png)

Mientras que en Machine Learning tenemos el siguiente:

![machineLearningFlujo.png](img/machineLearningFlujo.png)

La diferencia fundamental radica en que mientras en la programación tradicional se deben establecer de antemano las reglas que nos permiten obtener un determinado _output_ de un _input_, en Machine Learning estas reglas se infieren por medio de ejemplos.

El algoritmo de Machine Learning va a procurar generar una función matemática que se ajuste a los ejemplos proporcionados para así poder llegar de un _input_ a un _output_ sin tener que programar la lógica.

Este proceso por el cual el algoritmo genera esta función, lo vamos a llamar `entrenamiento del modelo`.

>Supongamos que tenemos que segmentar cervezas de acuerdo a sus características, podemos establecer cortes de las variables _IBU_ y _graduación alcohólica_ y formar grupos. Esto sería desde el enfoque de la `programación tradicional`.
<br>Resolver el mismo problema utilizando `Machine Learning` involucraría ejecutar el algoritmo para que la computadora "aprenda" de estos ejemplos y genere los segmentos en base a la estructura de los datos.

>Otro caso sería el de Turing y su equipo en El Código Enigma (por ahí fue en serio así, yo sólo vi la peli).
<br>En vez de buscar reglas a mano, con un algoritmo de Machine Learning podrían haberlas inferido.

## Tipos de Problemas

### Aprendizaje Supervizado

Supongamos que queremos predecir a qué artista pertenece una canción de acuerdo a la letra.

En los problemas de aprendizaje supervizado, cada observación tiene una variable de salida asociada la cual llamaremos `clase` o `target`. 

Se buscarán patrones que permitan, mediante el resto de las variables (de ahora en más las llamaremos `variables predictivas`), estimar el valor que toma la clase.

Este tipo de modelos son los llamados `modelos predictivos`.

En nuestro caso cada observación sería la canción y la clase es a qué artista pertenece. Las variables predictivas van a depender de la forma que usemos para representar el texto libre.

### Aprendizaje No Supervizado

Supongamos que sólo queremos agrupar las canciones de acuerdo a su letra.

En este caso no existe una variable a predecir y lo que los algoritmos harán es buscar patrones en los datos de forma de describir su estructura automáticamente.

### Preguntas

Qué tipo de problema es cada uno de los siguientes?

Segmentar las cervezas de acuerdo a su IBU, graduación alcohólica y cuerpo.

>Es un problema de aprendizaje no supervizado, en este caso no tenemos una clase asociada.

Recomendar una cerveza.

>Es un problema de aprendizaje supervizado, para poder predecir qué cerveza le puede gustar a una persona vamos a necesitar tener un conjunto de datos con características tanto de las personas como de las cervezas y la clase que indique si le gustó no.

Identificar factores que influyen en la calidad de una cerveza

>Es un problema de aprendizaje supervizado. En este caso si bien no vamos a intentar predecir, ya que queremos identificar los factores que influyen en la calidad, vamos a estar usando un modelo que prediga la calidad de la cerveza en base a sus características y nos quedaremos con la ecuación que nos devuelve el modelo. Más adelante ahondaremos en esto.

## Aprendizaje Supervizado

### Regresión

En los problemas de regresión lo que se está queriendo predecir es el valor de una `variable numérica`.

#### Casos de uso

__Predicción de Sueldos__

Un banco no conoce el _ingreso de sus clientes sin cuenta sueldo_.
<br>Lo que sí conoce, son los consumos, domicilio y otras variables de sus clientes, tanto los que cobran por este banco como los que no.
<br>Utilizando los datos de los _clientes que cobran en el banco_, ya que tiene información tanto del comportamiento (variables predictivas) como del sueldo (target), se entrena un modelo que, en base al comportamiento, prediga el sueldo para luego aplicarlo sobre los clientes que no cobran el sueldo en el banco.

__Estimación de la Demanda__

Un productor de cervezas artesanales quiere saber cuál va a ser su demanda para el próximo verano.
<br>Posee los datos de cuánto vendió mes a mes los últimos 7 años. Lo que quiere predecir son ventas en función de ventas.
<br>Lo que tenemos que distinguir es que una variable es la _cantidad demandada del próximo verano_ y otras variables son las _cantidades demandadas anteriormente_. Por lo tanto, el problema es _predecir ventas futuras en función de las ventas pasadas_.

__Extraer Insights__

Una investigadora quiere conocer cuáles son los factores que contribuyen al sobrepeso en las personas.
<br>Como no tiene posibilidad de hacer un ensayo controlado, lo que puede hacer es confeccionar una encuesta con diversas preguntas, entre las cuales se incluya el peso y la altura para calcular el IMC.
<br>Entrena un modelo que prediga el _IMC_ en función de las demás variables pero, a diferencia de los casos anteriores, no va a aplicar el modelo para predecir casos desconocidos.
<br>En este caso, lo que le va a interesar son los patrones hallados en los datos de cómo se correlaciona (varía conjuntamente) cada variable con el sobrepeso. Luego, puede realizar un experimento para determinar si hubo causalidad.

>Recordar siempre, la correlación NO implica causalidad.

### Clasificación

En este caso lo que se está queriendo predecir es el valor de una variable categórica.

A diferencia de los modelos de regresión, en los cuales vamos a obtener un número predicho, en estos modelos lo que obtenemos es una `probabilidad condicional` para cada valor posible de la clase para cada observación, dadas las variables predictivas.

Que una observación tenga una valor determinado a de clase se los llama `pertenecer a la clase`.

Si una clase tiene 2 valores posibles (Verdadero/Falso, Sí/No) se lo llama problema `binario`. Si son más valores posibles, se lo llama `multiclase`.

#### Ejemplos

__Predicción de Bajas__

Una empresa de telecomunicaciones quiere reducir los descuentos que otorga cuando un cliente llama a pedir la baja.
<br>Para ello, necesita identificar qué clientes tienen la intención de darse de baja realmente y quiénes llaman especulando para obtener un descuento.
<br>Tomando datos de 6 meses, se entrena un modelo que pueda distinguir aquellos que se dieron de baja efectivamente de aquellos que no.
<br>Mensualmente se envía al call center un puntaje de score para cada uno de los clientes así quien lo atiende sabe si tiene que ofrecer un descuento agresivo o bien un descuento menor.

__Predicción de No Pago__

Llamamos a nuestro banco a pedir que nos aumenten el límite de la tarjeta de crédito.
<br>Si bien pagamos siempre en término en este banco, nunca pagamos el resumen de la tarjeta de una tienda departamental porque cancelamos la compra pero nunca se revirtió el gasto en la misma.
<br>Nuestra oficial de cuentas nos dice que no pueden ampliarnos el límite porque tenemos un bajo score.
<br>En este caso, quien calculó la probabilidad de que nosotros no paguemos es un bureau de crédito, una institución que centraliza los datos de instituciones crediticas y les devuelve el score (entre otros servicios).

__Recomendación de Productos__

Entramos a Netflix y nos recomienda películas. Entramos a Spotify y tenemos una lista personalizada. Compramos algo en Amazon y nos llega una recomendación de otros productos.
<br>En este caso, los proveedores conocen nuestro comportamiento en sus plataformas y el de otras personas.
<br>En base a los ítems que fueron consumidos juntos, las características del ítem, y/o los consumos de personas con un consumo parecido al nuestro, nos va a recomendar otro ítem.

__Auto que se maneja solo__

Los autos que se manejan solos poseen un conjunto de reglas para actuar cuando observan algo en el tránsito.
<br>Para identificar qué es lo que sucede adelante suyo, los autos poseen algoritmos de reconocimiento de imagen que tienen las funciones de: identificar _dónde_ hay un objeto y _qué_ objeto es.
<br>Para poder entrenar el modelo, el algoritimo necesita una gran cantidad de imágenes etiquetadas manualmente.
<br>Quiénes están contantemente etiquetando imágenes?

>Nosotros al completar los Captcha.

## Workflow

### Definición del Problema

Todo proyecto de Machine Learning tiene que comenzar con un problema a resolver y una solución a implementar.

+ Quiero optimizar mis ventas
    + Con una fuerza de ventas de determinado tamaño quiero vender más
        + Envío un listado con los clientes ordenados de más propenso a menos propenso y que se llame en ese orden
    + Quiero reducir mi fuerza de ventas necesaria
        + Envío un listado con una cantidad fija de clientes
+ Quiero identificar a los morosos
    + Actuar antes de que caigan en mora
        + Genero una tabla en una base de datos con el puntaje de cada cliente
+ Identificar quiénes dan de baja el servicio
    + Ofrecer una promoción
        + Le paso el código del modelo al analista
+ Predecir el precio de un departamento
    + Disponibilizar una aplicación para que el cliente sepa si quiere vender o no
        + Genero una API
+ Identificar caras
    + Agrupar las fotos de una carpeta por contenido
        + Incorpora esta lógica a la aplicación

Estos son los problemas macro. Una vez definido el problema global, hay que definir unívocamente la clase:

+ Qué quiero optimizar de mis ventas? Cantidad o monto?
+ Qué es una venta? Hay algún tipo de restricción de tiempo?
+ El listado que se envíe qué vigencia tiene que tener?
+ A partir de qué mes se lo considera moroso?
+ Cómo se establece una baja?
+ Cuál es el precio que se toma? En qué moneda?

También se debe definir el alcance del modelo, a qué población va a afectar.

En este momento también se pueden establecer ciertas restricciones, por ejemplo:

+ _Meses de observación:_ dependiendo del problema, puede cambiar la ventana de observación
+ _Modelos a utilizar:_ algunos algoritmos devuelven un resultado más interpretable que otros y hay una restricción al uso
+ _Sistemas a utilizar:_ ya sea por licencias compradas, por conocimientos del equipo, se puede establecer que el modelo sea desarrollado en cierta arquitectura

### Extracción de los datos

Una vez definido el problema, vamos a pasar a la extracción de los datos de la fuente.

Dependiendo de la fuente, va a ser el tipo de extracción:

+ Base de Datos: query
+ Archivos Planos: servidor, pedido al responsable
+ Integración con sistema: script de automatización o bien generación manual
+ Internet: API o scrapping

Para la extracción de datos, tenemos que tener en cuenta el horizonte temporal de un modelo predictivo.

### Transformación de los Datos

#### Integrar

Unir las diferentes fuentes de datos.

#### Transformar

Modificar los datos para que cumplan nuestra necesidad.

##### Estructurar

La mayoría de las librerías de modelos predictivos trabajan con datos estructurados, correspondiendo cada fila a una observación y cada columna a una variable.

#### Limpiar

Eliminar o corregir registros erróneos.

#### Filtrar

Conservar sólo los registros que necesitamos.

#### Sumarizar

Resumir varios registros en uno por cada unidad de modelado.

#### Generación de Features

Creación de variables por medio de otras.

### Análisis Exploratorio de Datos

En esta estapa se realizará un análisis exploratorio de los datos con el fin de comprender mejor nuestro dataset.

### Modelado

#### Transformaciones

Previo a entrenar el modelo, algunos algoritmos requieren algunas transformaciones. De hecho, algunas transformaciones pueden considerarse parte del modelado.

##### Imputación de valores nulos

Algunos algoritmos trabajan bien con valores nulos, mientras que otros requieren una imputación de los mismos

##### One Hot Encoding

Consiste en pasar de una variable categórica a variables _dummy_, una por cada valor de categoría.

![OHE.png](img/OHE.png)

##### Transformaciones numéricas

Consiste en aplicar logaritmos, estandarizar (restar la media y dividir por el desvío standard), etc.

##### Discretización

Cortar las variables continuas en intervalos.

#### Entrenamiento

En esta etapa se prueban diversos algoritmos para tener un abanico de opciones en el momento de modelar.

### Evaluación

#### Métricas

Existen diferentes métricas y criterios de evaluación, lo más importante es utilizar la adecuada para `el problema que estamos resolviendo`.

#### Candidatos

De acuerdo al criterio de selección utilizado, vamos a tomar 1 o más modelos candidatos.

### Testing

Antes de ponerlos en producción, utilizando un nuevo conjunto de datos, vamos a ver cuál es el desempeño de nuestro modelo y seleccionar el final.

### Puesta en Producción

#### Programación de la lógica en otro lenguaje

Los lenguajes de programación utilizados en Data Science, no siempre son los más eficientes, dado que están pensados para ser utilizados a alto nivel.

Es por ello que, dependiendo del uso que se le vaya a dar, sea necesario programar el scoring (aplicación del modelo) en un lenguaje más performante.

#### Automatización

Lo ideal es que la aplicación del modelo requiera la menor intervención humana posible, por lo cual entre otras cosas de debe:

* Reemplazar valores hard-codeados por variables
* Limpiar el script para que no haga cálculos innecesarios
* Garantizar que el script corre secuencialmente

### Seguimiento

Regularmente, se debe evaluar el resultado del modelo, no sólo en términos de Machine Learning, sino de resultado del negocio.

### Recalibración

Una vez que el modelo pierde su capacidad predictiva, se procede a entrenar nuevamente el modelo

Esto se puede deber a:
* La tasa de clasificación se acerca a la de una muestra al azar.
* Las variables numéricas (por ejemplo de precio) quedan desfasadas (por inflación, aumento del consumo, etc.).
* Eventos temporales modifican valores “normales” de las variables.