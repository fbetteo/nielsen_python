# Scikit-Learn

## Qué es Scikit-Learn?

`Scikit-Learn` es la librería de Python por excelencia para hacer Machine Learning.

Posee herramientas para Aprendizaje Supervizado, Aprendizaje no Supervizado, Selección de Modelos, Evaluación, Transformación de Datos y Pipelines.

## Ejemplo

Veamos un ejemplo de un flujo de trabajo.

### 1. Importamos los datos

```python
import pandas as pd
import os
df = pd.read_csv(os.path.join('data','wine.csv'))
```

`Scikit Learn` trabaja con arrays, uno con las variables predictivas, usualmente llamado `X` y uno con el target llamado `y`.

```python
X = df.iloc[:,:-1].values
y = df.iloc[:,-1].values
```

>_X_ es un `numpy.array` de __2 dimensiones__ que contiene las variables predictivas. Cada fila corresponde a una observación y cada columna a una variable.
><br>_y_ es un numpy.array de __1 dimensión__ con el valor del target.

### 2. Importamos los objetos que vamos a utilizar

```python
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier 
```
### 3. Separamos en Training y Testing (más detalles más adelante)

```python
X_train,X_test,y_train,y_test = train_test_split(X,y)
```

### 4. Preprocesamos

```python
std = StandardScaler(with_mean=False)

std.fit(X_train)

X_train_std = std.transform(X_train)

```

### 5. Entrenamos el modelo

```python
clf = DecisionTreeClassifier()
clf.fit(X_train_std,y_train)
```

### 6. Aplicamos el Modelo

```python
X_test_std = std.transform(X_test)
prediction = clf.predict(X_test_std)
```