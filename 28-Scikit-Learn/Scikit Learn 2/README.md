#  Scikit-Learn 2

## Flujo de Trabajo

### Estructura de la Librería

La librería `sklearn` está compuesta por varios módulos y clases, cada uno con una finalidad en particular.

Podemos encontrar un listado completo en la documentación oficial: [http://scikit-learn.org/stable/modules/classes.html](http://scikit-learn.org/stable/modules/classes.html)

### Importación del objeto

Como convención, al trabajar con sklearn se importa el objeto sin alias.

Ejemplo:

```python
from sklearn.preprocessing import StandardScaler
```

>En este caso estamos importando el objeto que nos va a permitir hacer una estandarización.

Vamos a tener 2 tipos de objetos:

1. __Transformadores:__ Son los que van a modificar los datos para el modelado.
    * Poseen 2 métodos principales
        * _fit:_ Calcular las medidas a utilizar para transformar (ejemplo: la media y el desvío standard en caso de escalar)
        * _transform:_ Aplicar la transformación.
2. __Entrenadores:__ Son los correspondientes a la lógica funcional del modelo
    * Poseen 2 métodos principales
        * _fit:_ Entrenar el modelo.
        * _predict:_ Aplicar el modelo.

### Instanciar el objeto

Para trabajar, tenemos que instanciar lo que acabamos de importar con las opciones.

Ejemplo:

```python
std = StandardScaler(with_mean=False)
```

>En este caso vamos a escalar la variable con respecto a su desvío standard, pero no la vamos a centrar a la media.

### Método _fit_

El método _fit_ sirve para calcular los parámetros de la transformación en el caso de los transformadores o para entrenar el modelo.

Se pasa como argumento, en caso de los transformadores, el dataset con las variables a transformar, en caso de los entrenadores, un dataset con las variables predictivas y, si es un método supervizado, un array con la clase.

Ejemplo:

```python
std.fit([[1,4,5],[3,6,2],[2,7,8],[9,3,0]])
```

El resultado de esto, es un cambio en las propiedades del objeto.

### Método *fit_transform*

La diferencia entre _fit_ y *fit_transform* es que este último devuelve también el resultado de aplicar el método.

Ejemplo:

```
In : std.fit_transform([[1,4,5],[3,6,2],[2,7,8],[9,3,0]])
Out: 
array([[0.32128773, 2.52982213, 1.6495722 ],
       [0.96386319, 3.79473319, 0.65982888],
       [0.64257546, 4.42718872, 2.63931552],
       [2.89158958, 1.8973666 , 0.        ]])
```

### Método _transform_

Este método sirve para aplicar la transformación a un nuevo dataset.

Ejemplo:

```python
In : std.transform([[3,5,9],[4,8,0]])
Out: 
array([[0.96386319, 3.16227766, 2.96922996],
       [1.28515093, 5.05964426, 0.        ]])

```

### Método _predict_

Este método aplica el modelo a un nuevo conjunto de datos.

El argumento que toma es el conjunto de datos sobre el cual se quiere aplicar el modelo.

## Sesgo-Varianza

![SesgoVarianza.png](img/SesgoVarianza.png)

Cuando hacemos un modelo, no tenemos que perder de vista cuál es el objetivo: Predecir nuevas observaciones.

Cuando un modelo no se ajusta a las observaciones del dataset usado para entrenar, decimos que tiene alto `sesgo` o bien, que estamos teniendo `underfitting`.

Lo opuesto sucede cuando nuestro modelo se ajusta demasiado al dataset utilizado y luego no puede generalizar bien. En ese caso nos encontramos con un modelo de alta `varianza`, estamos teniendo `overfitting`.

### Training - Validation - Testing

Para simular un escenario real, vamos a dividir las observaciones de nuestro dataset en _al menos_ 2 conjuntos de datos:

1. __Training:__ Conjunto de datos que vamos a utilizar para entrenar el modelo.
2. __Validation:__ Conjunto de datos que vamos a utilizar para evaluar la performance del modelo.
3. __Testing:__ Conjunto de datos que vamos a utilizar, una vez seleccionado el mejor modelo con _Validation_, para evaluar cómo se comportaría en un escenario real.
<br>Una práctica usual es utilizar un conjunto de datos de otro período.

#### Criterios de División

Tomemos el ejemplo de nuestro dataset `wine`. Las cantidades y proporciones originales son:

| valores | cantidad original | proporción original |
|---------|-------------------|---------------------|
|red|1599|0.246114|
|white|4898|0.753886|

##### Muestra aleatoria 

Se toma una muestra completamente aleatoria de las observaciones, independientemente del valor de la clase.

Supongamos que tomamos un 70% de los casos para entrenamiento, sobre nuestro conjunto `wine`, obtenemos lo siguiente:

| valores | cantidad original | proporción original | cantidad con muestra aletoria | proporción con muestra aleatoria |
|---------|-------------------|---------------------|------|----------|
|red|1599|0.246114|1115|0.245217|
|white|4898|0.753886|3432|0.754783|

##### Manteniendo distribución de la clase 

Se toma una muestra estratificada, de forma tal de mantener la proporción de casos de cada valor de la clase en _Training_ y _Validation_.

Tomando el 70% en wine obtenemos:

| valores | cantidad original | proporción original | cantidad con muestra estratificada | proporción con muestra estratificada |
|---------|-------------------|---------------------|------|----------|
|red|1599|0.246114|1119|0.246096|
|white|4898|0.753886|3428|0.753904|

##### Over/Under Sampling

Algunos algoritmos tienen problemas cuando la clase está desbalanceada, es decir, cuando la proporción de un valor de la clase es ampliamente mayor a la de otro valor.

Es por ello que existen formas de rebalancear la muestra.

En el caso de `undersampling` se tomará una proporción menor de observaciones correspondientes al valor de la clase mayoritario de modo de balancear la muestra.

En el caso de `oversampling` se extraerá con reemplazo una proporción mayor de observaciones del valor minoritario de la clase.

### Variables Categóricas

Debido a que la librería scikit-learn va a trabajar con matrices numéricas, al tener una variable categórica, la tenemos que pasar a numérica.

La tranformación va a depender del tipo de modelo y del tipo de variable.

#### One Hot Encoding

Tomamos las variables categóricas y generamos una variable dummy para cada valor.

Un DataFrame con la siguiente forma:

| Sexo | Edad | Ocupacion  |
|------|------|------------|
| M    | 23   | Estudiante |
| F    | 59   | Docente   |
| F    | 45   | Albañil     |

Hay que expresarlo como:

| Sexo | Edad | Ocupacion_Estudiante | Ocupacion_Docente | Ocupacion_Albañil |
|------|------|----------------------|--------------------|------------------|
| 1    | 23   | 1                    | 0                  | 0                |
| 0    | 59   | 0                    | 1                  | 0                |
| 0    | 45   | 0                    | 0                  | 1                |

### Imputación Valores Nulos

En el dataset puede haber valores nulos.

Esto se puede deber a:

* Errores en la recolección de datos
* Campo no disponible para la observación debido a la naturaleza del campo
    * Ejemplo: Situación laboral para un menor de edad
* Campo no disponible para la observación debido a que el individuo no desea manifestar el valor
    * Ejemplo: Una persona con alto ingreso no desea responder la pregunta
* El campo es calculado y el cálculo con los valores no es posible
    * Ejemplo: Una tasa con un denominador igual a 0

La mayoría de los algoritmos no puede trabajar con valores nulos
Sin embargo, el valor nulo puede tener valor predictivo

Ejemplo:

* Si uno de los campos es “ingreso”, que el entrevistado se niegue a
responder puede ser explicativo de la variable a predecir
* Si un cociente es nulo, se debe a que el denominador es 0

Por lo tanto, una de las discusiones más grandes en Data Science es imputar o no los valores nulos.

Scikit-Learn no permite el trabajo con valores nulos, por lo cual, es obligatorio imputarlos.

#### Criterios de Imputación

##### Criterio Personal

Para poder imputar los nulos utilizando un criterio personal se debe
tener conocimiento del conjunto de datos, del negocio y del modelo a
utilizar.

A qué es equivalente un valor nulo? A un 0?

Quiero que el valor nulo sea menor que el resto de los valores, que
sea mayor? Cuál es el efecto de esto?

##### Medida Estadística

Se puede reemplazar los valores nulos por la media, la mediana o
alguna medida estadística de la variable calculada sobre los valores no
nulos.

La desventaja de utilizar este método es que se está dando un valor no
real y, por otro lado, se afecta la distribución de la variable.

##### Imputación basada en modelo

Mediante este método, se utiliza un modelo para predecir cuál podría
ser el valor faltante.

La desventaja de este método es que puede requerir mucho poder de
cómputo y llevar mucho tiempo.

## Árboles de Decisión

### Objetivo

El `árbol de decisión` es un modelo predictivo que se basa en particionar el conjunto de datos sucesivamente para generar reglas de tipo `if-then`.

Por ejemplo, una regla pata el conjunto de datos `wine`:

si `total sulfur dioxide` <= 67.5
<br>y `chlorides` <= 0.04649999737739563
<br>entonces `P(Vino Tinto)`=0.11 y `P(Vino Blanco)`=0.89

Las observaciones que cumplan con determinada condición van a tener la probabilidad de pertenencia a la clase asociada a dicha regla.

La ventaja de este tipo de modelos es la posibilidad de representar gráficamente la estructura, por lo cual es fácilmente entendible.

![Árbol de Decisión](img/Arbol.png)

### Clase

El módulo donde se encuentra es `sklearn.tree` y la clase es `DecisionTreeClassifier`.

### Documentación

La referencia de la api se encuentra en [http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html](http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html) y la guía de usuario con tutorial y explicación del modelo en [http://scikit-learn.org/stable/modules/tree.html](http://scikit-learn.org/stable/modules/tree.html).


### Parámetros

Algunos parámetros que permite modificar:

#### criterion

El criterio para medir la impureza del nodo.

Las opciones son `"gini"` o `"entropy"`.

La opción por defecto es _"gini"_

#### max_depth

La profundidad máxima del árbol.

Tener en cuenta que cuanto más profundo más se va a ajustar a mis datos, pero corro mayor riesgo de overfitting.

Puede ser un número o `None`. En este último caso, continuará hasta encontrar el nodo que ya no abra.

La opción por defecto es _None_.

#### min\_samples\_split

La cantidad mínima de observaciones que tiene que poseer un nodo para intentar una partición.

Tener en cuenta que cuanto menor sea, más se va a ajustar a mis datos, pero corro mayor riesgo de overfitting.

Si es un `número entero`, es la cantidad que tiene que tener, si es un `número entre 0 y 1`, es la proporción de las observaciones.

La opción por defecto es _2_.

#### min\_samples\_leaf

La cantidad mínima de observaciones que tiene que tener cada nodo.

Tener en cuenta que cuanto menor sea, más se va a ajustar a mis datos, pero corro mayor riesgo de overfitting.

Si es un `número entero`, es la cantidad que tiene que tener, si es un `número entre 0 y 1`, es la proporción de las observaciones.

La opción por defecto es _1_.

#### min\_impurity\_decrease

El mínimo cambio en la impureza para partir el nodo.

La opción por defecto es _0_.

#### random\_state

En computación, no existen los números realmente aletorios. Lo que tenemos, son secuencias de números.

En caso de nosotros querer obtener el mismo resultado todas las veces que ejecutemos un código que contenga cierta aleatoriedad, tenemos que establecer una semilla aleatoria para indicar dónde queremos que comience la secuencia.

Este parámetro sirve para esto. El valor por defecto es `None` indicando que no va a estar fijo.

## Regresión Logística

### Objetivo

La `regresión logística` es un método que procura predecir la probabilidad de pertenencia a una clase por medio de una función matemática.

### Problemas binarios

#### Función

La probabilidad estimada para una observación es:

![logisticabinaria.gif](img/logisticabinaria.gif)

Donde:
* _x<sub>i</sub>_: El valor de cada una de las variables predictivas para un observación.
* _β<sub>0</sub>_: Intercepto (un término que se suma).
* _β<sub>i</sub>_: Coeficiente de cada variable (un valor que se multiplica).

#### Logit

Despejando la función obtenemos:

![logit.gif](img/logit.gif)

Lo cual llamaremos `logit`.

Lo que se encuentra entre paréntesis se llama `odds-ratio`, traducible como _chance_, que sería el ratio entre casos positivos y casos negativos. 

Si bien nosotros no estamos acostumbrados a pensar en esta medida, sino en proporciones, en algunos países están más familiarizados con ésta.

#### Variables categóricas

Como la fórmula requiere que las variables sean de escala (poder medir la distancia entre los valores), se deben usar variables dummy independientemente de si son ordinales o no.

#### Ejemplo

Si tratamos de predecir la probabilidad de que un vino sea blanco, los coeficientes del modelo son:

* fixed acidity: -0.6594340588343609
* volatile acidity: -9.014401892325202
* citric acid: 0.41327183937014156
* residual sugar: 0.15358398256937378
* chlorides: -2.600927200816092
* free sulfur dioxide: -0.04358356549254807
* total sulfur dioxide: 0.0629957805447258
* density: 3.5254976150485198
* pH: -2.140686944207208
* sulphates: -6.869699028336532
* alcohol: 0.7142926517457676
* quality_3: 1.035367460229071
* quality_4: 1.9959808518111266
* quality_5: -0.2413836878659183
* quality_6: 0.1498018197851874
* quality_7: 0.28169062683054863

Intercepto: 3.80996167

Para una observación que tenga los siguientes valores:

* fixed acidity: 7.5
* volatile acidity: 0.5
* citric acid: 0.36
* residual sugar: 6.1
* chlorides: 0.071
* free sulfur dioxide: 17.0
* total sulfur dioxide: 102.0
* density: 0.9978
* pH: 3.35
* sulphates: 0.8
* alcohol: 10.5
* quality_3: 0.0
* quality_4: 0.0
* quality_5: 1.0
* quality_6: 0.0
* quality_7: 0.0
 
El logit para esta observación entonces es:

```
3.809961668824377
+ (-0.6594340588343609) * 7.5
+ (-9.014401892325202) * 0.5
+ (0.41327183937014156) * 0.36
+ (0.15358398256937378) * 6.1
+ (-2.600927200816092) * 0.071
+ (-0.04358356549254807) * 17.0
+ (0.0629957805447258) * 102.0
+ (3.5254976150485198) * 0.9978
+ (-2.140686944207208) * 3.35
+ (-6.869699028336532) * 0.8
+ (0.7142926517457676) * 10.5
+ (1.035367460229071) * 0.0
+ (1.9959808518111266) * 0.0
+ (-0.2413836878659183) * 1.0
+ (0.1498018197851874) * 0.0
+ (0.28169062683054863) * 0.0
+ (0.4851779709073358) * 0.0
+ (0.1033266271271332) * 0.0
```

Entonces el valor de la probabilidad estimada de que sea un vino blanco de acuerdo a las observaciones va a ser:

1/(1+e<sup>-(-0.9480012)</sup>)=0.27928697

### Clase

El módulo donde se encuentra en `sklearn.linear_model` y la clase es `LogisticRegression`.

### Documentación

La referencia de la api se encuentra en [http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html) y la guía de usuario con tutorial y explicación del modelo en [http://scikit-learn.org/stable/modules/linear_model.html#logistic-regression](http://scikit-learn.org/stable/modules/linear_model.html#logistic-regression).

### Parámetros

Algunos parámetros que permite modificar:

#### penalty

El tipo de regularización usada.

Las opciones son `"l1"` o `"l2"`. Están explicadas en la [User Guide](http://scikit-learn.org/stable/modules/linear_model.html#logistic-regression).

La opción por defecto es _"l2"_

#### fit_intercept

Incluir _b<sub>0</sub>_.

Es un boolean y su valor por defecto es _True_.

#### solver

El algoritmo de resolución del problema de minimizar la función de costo.

#### multi_class

En caso de tener una clase con más de 2 valores diferentes, qué estrategia usar:

* _"ovr"_: One Vs Rest
    * Entrenar un modelo por valor de la clase contra las demás.
* _"multinomial"_: Multinomial
    * Entrenar un modelo que minimiza el log-loss de todos los valores de la clase.