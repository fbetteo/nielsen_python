# Markdown

`Markdown` es una forma de escribir y dar formato al texto.

## Headers

Anteponiendo sucesivos `#` al texto se crean encabezados.

```markdown
# Header 1
## Header 2
### Header 3
#### Header 4
```

># Header 1
>## Header 2
>### Header 3
>#### Header 4

## Inline Code

Poniendo el texto entre ` se pone un código entre el texto.

```mardown
`código`
```

>`código`

## Cursiva

Poniendo el texto entre `_` o entre `*` se pone el texto en cursiva.

```mardown
_cursiva_
```

>_cursiva_

## Negrita

Poniendo el texto entre `__` o entre `**` se pone el texto en negrita.

```markdown
__negrita__
```

>__negrita__

## Lista desordenada

Poniendo texto en renglones seguidos después de `*` crea una lista sin orden.

```markdown
* Elemento 1
* Elemento 2
    * Subelemento 1
* Elemento 3
```

>* Elemento 1
>* Elemento 2
>   * Subelemento 1
>* Elemento 3

## Lista ordenada

Poniendo texto después de `numero.` se crea una lista ordenada.

```markdown
1. Primero
2. Segundo
    1. subelemento
3. Tercero
```

>1. Primero
>2. Segundo
>    1. subelemento
>3. Tercero

## Links

Con la sintaxis `[Texto](destino)` crea un link.

```markdown
[Google](http://www.google.com)
```

>[Google](http://www.google.com)

## Imágenes

Con la sintaxis `![Texto Alternativo](Ubicación de la Imagen)` se inserta una imagen.

```markdown
![Perrito](https://i2.wp.com/contextodiario.com/wp-content/uploads/2017/12/que-comen-los-perros.jpg?w=800)
```

> ![Perrito](https://i2.wp.com/contextodiario.com/wp-content/uploads/2017/12/que-comen-los-perros.jpg?w=800)

## Código

Escribiendo código encerrado por ``` el intérprete de markdown puede colorear la sintaxis.

```python
print("Hola Mundo")
```

## Tablas

La sintaxis de las tablas es un poco molesta de hacer desde cero, así que es mejor algún sitio generador de tablas en markdown como [https://www.tablesgenerator.com/markdown_tables](https://www.tablesgenerator.com/markdown_tables).

```markdown
| Columna 1 | Columna 2 | Columna 3 |
|-----------|-----------|-----------|
| 1         | 2         | 3         |
| 4         | 5         | 6         |
```

>| Columna 1 | Columna 2 | Columna 3 |
>|-----------|-----------|-----------|
>| 1         | 2         | 3         |
>| 4         | 5         | 6         |

## Blockquotes

Poniendo texto seguido de `>` se genera una blockquote.

```markdown
>Esto es una blockquote
>Que sigue Acá

Acá se rompe

>Entonces acá empieza una nueva
```

>Esto es una blockquote
>Que sigue Acá

Acá se rompe

>Entonces acá empieza una nueva

Si quiero agregar una nueva línea en la blockquote tengo que usar el tag html `<br>`.

```markdown
>Esto es una blockquote
><br>Que sigue Acá

Acá se rompe

>Entonces acá empieza una nueva
```

>Esto es una blockquote
><br>Que sigue Acá

Acá se rompe

>Entonces acá empieza una nueva