# Variables

## Definición

Una variable es una ubicación en memoria asociado a un elemento.

## Asignación

La asignación se realiza con el operador `=` poniendo a la izquierda el nombre de la variable y a la derecha el valor a asignar.

```python
uno=1
```

### Reglas

* Los nombres de variables tienen que empezar con una `letra` o bien con `_` 
* Pueden tener números
* No pueden ser nombres reservados
* Son CASE SENSITIVE (No es lo mismo `Uno` que `uno`)

### Estilos

* camelCase
* UpperCamelCase
* CAPITALIZED_WITH_UNDERSCORES
* lowercase_separated_by_underscores

### Asignación múltiple
Podemos asignar más de una variable en la misma línea separando las asignaciones por coma:

```python
dos,tres=2,3
```

## Uso

Para utilizar la variable, basta con escribir su nombre:

```python
print(uno)
```

## Ver tipo de dato

Con la función `type` podemos ver el tipo de objeto:

```python
type(uno)

```

## Listar Propiedades y Métodos

Si queremos saber qué propiedades y métodos posee el objeto, utilizamos la función `dir`:

```python
dir(uno)
```

## Asignación por valor

Cuando una variable topa el valor de otra variable, si es un tipo de dato básico (Número, String o Boolean), se hace una copia de la variable.

```python
a=1
b=a
b=2
print(a)
```
