print(
'''
PRICING INSIGHTS --- Final Delivery Tool

****************************************
****************************************
''')


# Import libraries
import os
import csv
import pandas as pd
import numpy as np

import warnings
warnings.filterwarnings("ignore")

# Retrieve current working directory
cwd = os.getcwd()
print('-- Retrieving current working directory: {}'.format(cwd))


currency = input('-- What is the currency for this project?: ')






# Read files Input File
file_name = input('-- Please, enter the input file name (csv file): ')
df = pd.read_csv(file_name, encoding=r'latin_1', header='infer', index_col=None , low_memory=False)



#######################################
############ LOAD SUMMARIES ###########
#######################################

summaries_input = input('-- Do you want to calculate summaries? (y/n): ')

if summaries_input == 'y':
	'''
	Weight Average Functions
	'''
	price_strat_reg = input('\t-- Please, enter the Regular Price Strategy parameter: ')
	price_strat_promo = input('\t-- Please, enter the Promoted Price Strategy parameter: ')

	def w_average_value_market(series):
		dropped = series.dropna()
		dropped = dropped[dropped != 0]
		try:
			return np.average(dropped, weights=df.loc[dropped.index, 'total_value_market'])
		except:
			return np.average(dropped)

	def w_average_value_cli(series):
		dropped = series.dropna()
		dropped = dropped[dropped != 0]
		try:
			return np.average(dropped, weights=df.loc[dropped.index, 'total_value_cli'])
		except:
			return np.average(dropped)

	def w_average_value_rem(series):
		dropped = series.dropna()
		dropped = dropped[dropped != 0]
		try:
			return np.average(dropped, weights=df.loc[dropped.index, 'total_value_rem'])
		except:
			return np.average(dropped)

	def w_average_units_market(series):
	    dropped = series[series != 0].dropna()
	    try:
	    	return np.average(dropped, weights=df.loc[dropped.index, 'total_units_market'])
	    except:
	    	return np.average(dropped)

	def w_average_units_cli(series):
	    dropped = series[series != 0].dropna()
	    try:
	    	return np.average(dropped, weights=df.loc[dropped.index, 'total_units_cli'])
	    except:
	    	return np.average(dropped)

	def w_average_units_rem(series):
	    dropped = series[series != 0].dropna()
	    try:
	    	return np.average(dropped, weights=df.loc[dropped.index, 'total_units_rem'])
	    except:
	    	return np.average(dropped)

	###############################
	###							###
	###	 	Decile Summary  	###
	###							###
	###############################
	
	deciles_agg_funcs = {
		'PI_regular' : w_average_value_market,		
		'PI_promo' 	: w_average_value_market,
		'UPC Tag' : 'count',
		'total_value_market' : 'sum',
		'total_value_cli' : 'sum',
		'total_value_rem' : 'sum',
	}
	
	sum_dec = df.groupby(['Trading Area Name' , 'Market decile']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	###############################
	###							###
	###	 	Decile/Cat Summary  ###
	###							###
	###############################
	sum_dec_cat = df.groupby(['Trading Area Name' , 'Market decile' , 'Category Name']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	
	###############################
	###							###
	###	 Regular Boxes Summary  ###
	###							###
	###############################
	sum_reg_box = df.groupby(['Trading Area Name' , 'box_regular']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	###############################
	###							###
	###	 Promo Boxes Summary    ###
	###							###
	###############################
	sum_promo_box = df.groupby(['Trading Area Name' , 'box_promo']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	#####################################
	###								  ###
	###	 Regular/Cat Boxes Summary    ###
	###							      ###
	#####################################
	cat_boxes_funcs = {
		'PI_regular' : 'mean',		
		'PI_promo' 	: 'mean',
		'UPC Tag' : 'count',
		'total_value_market' : 'sum',
		'total_value_cli' : 'sum',
		'total_value_rem' : 'sum',
	}
	sum_reg_box_cat = df.groupby(['Trading Area Name' , 'box_regular' , 'Category Name' ]).agg(cat_boxes_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	#####################################
	###								  ###
	###	 Promo/Cat Boxes Summary      ###
	###							      ###
	#####################################
	sum_promo_box_cat = df.groupby(['Trading Area Name' , 'Category Name' , 'box_promo']).agg(cat_boxes_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	###############################
	###							###
	###	 	Category Summary    ###
	###							###
	###############################
	category_agg_funcs = {
		'Client Regular Elasticity' : w_average_units_cli,
		'Remaining Regular Elasticity' : w_average_units_rem,
		'Client Promo Elasticity' : w_average_units_cli,
		'Remaining Promo Elasticity' : w_average_units_rem,
		'PI_regular' : w_average_value_market,		
		'PI_promo' 	: w_average_value_market,
		'Discount Frequency_cli' : w_average_value_cli,
		'Discount Frequency_rem' : w_average_value_rem,
		'Discount Depth_cli' : w_average_value_cli,
		'Discount Depth_rem' : w_average_value_rem,
		'UPC Tag' : 'count',
		'total_value_market' : 'sum',
		'total_value_cli' : 'sum',
		'total_value_rem' : 'sum',
		'total_value_promo' : 'sum',
		'total_value_reg' : 'sum',
		'total_value_promoted_cli' : 'sum',
		'total_value_promoted_rem' : 'sum',
	}


	sum_cat = df.groupby(['Trading Area Name' , 'Category Name']).agg(category_agg_funcs).reset_index()\

	sum_cat['Price Strategy Client'] = 'Otro'
	sum_cat['Price Strategy Remaining'] = 'Otro'
	sum_cat.loc[(sum_cat['Client Regular Elasticity'] <= float(price_strat_reg)) & \
				(sum_cat['Client Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Client'] = 'Price Fighter'

	sum_cat.loc[(sum_cat['Client Regular Elasticity'] >= float(price_strat_reg)) & \
				(sum_cat['Client Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Client'] = 'Hi-No'

	sum_cat.loc[(sum_cat['Client Regular Elasticity'] <= float(price_strat_reg)) &	\
				(sum_cat['Client Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Client'] = 'EDLP'

	sum_cat.loc[(sum_cat['Client Regular Elasticity'] >= float(price_strat_reg)) &	\
				(sum_cat['Client Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Client'] = 'Hi-Low'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] <= float(price_strat_reg)) & \
				(sum_cat['Remaining Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'Price Fighter'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] >= float(price_strat_reg)) & \
				(sum_cat['Remaining Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'Hi-No'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] <= float(price_strat_reg)) &	\
				(sum_cat['Remaining Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'EDLP'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] >= float(price_strat_reg)) &	\
				(sum_cat['Remaining Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'Hi-Low'

	sum_cat['Promo %'] = sum_cat['total_value_promo'] / sum_cat['total_value_market']
	sum_cat['Promo Client %'] = sum_cat['total_value_promoted_cli'] / sum_cat['total_value_cli']
	sum_cat['Promo Remaining %'] = sum_cat['total_value_promoted_rem'] / sum_cat['total_value_rem']

	sum_cat = sum_cat.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
							'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining',
							'total_value_promo': 'Total Promoted Value' , 'total_value_reg' : 'Total Regular Value' ,
							'total_value_promoted_cli' : 'Total Client Promoted Value' , 'total_value_promoted_rem' : 'Total Remaining Promoted Value',
						})
	

else:
	sum_dec = pd.read_csv('summary_deciles.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_dec_cat = pd.read_csv('summary_deciles_cat.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_cat = pd.read_csv('summary_category.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_reg_box = pd.read_csv('summary_boxes_regular.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_reg_box_cat = pd.read_csv('summary_boxes_regular_cat.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_promo_box = pd.read_csv('summary_boxes_promo.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_promo_box_cat = pd.read_csv('summary_boxes_promo_cat.csv', encoding=r'latin_1', header='infer', index_col=None)	

