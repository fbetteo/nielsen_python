# Excepciones

Siempre que ocurre un error en tiempo de ejecución, se crea una excepción. Normalmente el programa se para y Python presenta un mensaje de error.

Por ejemplo, la división por cero crea una excepción:

```python
print(55/0)
```

Un elemento no existente en una lista hace lo mismo:

```python
a = []
print(a[5])
```

O el acceso a una clave que no está en el diccionario:

```python
b = {}
print(b['qué'])
```

El mensaje de error tiene dos partes: 

* El tipo de error antes de los dos puntos
* Detalles sobre el error después de los dos puntos 

Normalmente, Python también imprime una traza de dónde se encontraba el programa al fallar.

Podemos manejar la excepción usando las sentencias `try` y `except`.

```python
nombreArch = input('Introduce un nombre de archivo: ')
try:
    with open(nombreArch) as f:
        pass
except:
    print('No existe el archivo {}'.format(nombreArch))
```

La sentencia `try` ejecuta las sentencias del primer bloque. Si no se produce ninguna excepción, pasa por alto la sentencia `except`. Si ocurre cualquier excepción, ejecuta las sentencias de la rama `except` y después continúa.

Se pueden manejar diferentes excepciones por tipo de error.

```python
lista_pedorra = [1,0,'A']

for i in lista_pedorra:
    try:
        print('{}: {}'.format(i,1/i))
    except ZeroDivisionError:
        print('{}: {}'.format(i,"No puedo dividir por 0"))
    except TypeError:
        print('{}: {}'.format(i,"No puedo dividir por algo que no sea un número"))
```

## Ejercicio

Tomar el archivo `formateador_PI Mirek_Parte_I.py`, analizarlo y modificarlo para aplicar lo visto hasta ahora y de la forma que crean va más con su estilo.

El input para ese archivo está en la carpeta `data` del repositorio y se llama `PI_Export Agniezka.csv`
