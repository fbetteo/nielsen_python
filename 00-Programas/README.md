# Programas a Instalar

## Git

`Git` es un sistema de control de versiones.

Permite llevar un control de los cambios en el código y de esas formas evitar los archivos con nombre "Assortman_Final.xlsx", "PnP_FinalFinal.pptx" o "Coca-Cola este es el bueno.sas".

En nuestro caso lo vamos a usar sólo para compartir el material del curso, pero está bueno que lo vayan conociendo por si se topan con algún proyecto en GitHub o GitLab.

### Descarga

[https://git-scm.com/](https://git-scm.com/)

## Anaconda

Es una distribución de Python, contiene, además de Python, varias librerías e IDEs ya instaladas.

### Descarga

[https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/)

## Visual Studio Code

Es un editor de código multiplataforma creado por Microsoft (ya fue la de ser anti Bill Gates después de ver lo que era Steve Jobs).

Tiene varios plugins y se actualiza todos los meses, no sólo sirve para Python, sino para varios otros lenguajes.

### Descarga
[https://code.visualstudio.com/](https://code.visualstudio.com/)