alumnado = {'nombres':['Ariana Macarena Scasserra',
'Melina Terreno',
'Micaela Ahedo',
'Micaela Gimenez',
'Tomas Pallotti',
'Franco Betteo',
'Agustina Alzola',
'Juan Tobares',
'Ezequiel Prada'],
'mails':[
'arianamacarena.scasserra@nielsen.com',
'melina.terreno@nielsen.com',
'micaela.ahedo@nielsen.com',
'micaela.gimenez@nielsen.com',
'tomas.pallotti@nielsen.com',
'franco.x.betteo@nielsen.com',
'agustina.alzola@nielsen.com',
'juan.tobares@nielsen.com',
'ezequiel.prada@nielsen.com'
]}

saludo = ', '.join(alumnado['nombres'][:-1]) + ' y ' + alumnado['nombres'][-1]

print('Hola {s}!!! Cómo andan?'.format(s=saludo))