# Diccionarios

Un diccionario es una estructura de `clave-valor` que permite acceder fácil y rapidamente a elementos por medio de un índice.

## Creación

### llaves

```python
llaves = {'uno':1,
          'otra variable':2,
          3:45}
```

Como podemos observar, entre `{}` se colocan los pares clave:valor. Por otro lado, las claves pueden ser de diferentes tipos.

### dict

También se puede crear un diccionario con la función `dict`.

```python
funcion = dict(uno=1,
               dos=2,
               tres=3)
```

En este caso, las claves tienen que cumplir con las reglas de las variables de Python.

## Obteniendo elementos

Para obtener el valor de un diccionario simplemente se pone escribe el nombre de la variable y entre corchetes se coloca la clave.

```python
llaves['uno']
```

Los valores de las claves pueden estar dados por una variable también.

```python
foo='otra variable'
llaves[foo]
```

### get

Cuando le pedimos una clave que no está en el diccionario por medio de las llaves nos va a devolver un error.

En caso de querer que nos devuelva un valor por defecto de no encontrar la clave, usamos la función `get` del diccionario con la sintaxis `<diccionario>.get(<clave>,<valor por defecto>)`

```python
llaves.get('cuatro','No encontré nada')
```

## Modificando el diccionario

### Agregando elementos

Para agregar un elemento, se escribe entre corchetes el valor de la clave y se le asigna un valor a ese elemento.

```python
llaves['calabaza']='qwerty'
```

### Modificando elementos

Para modificar un elemento, se escribe entre corchetes el valor de la clave y se asigna el nuevo valor a ese elemento.

```python
llaves[3]='zaraza'
```

### Eliminando elementos

Con la sentencia `del` se elimina la clave deseada.

```python
del llaves['uno']
```

## keys

Con el método `keys` se obtienen las claves del diccionario.

```python
llaves.keys()
```

## values

Con el método `values` se obtienen los valores del diccionario.

```python
llaves.values()
```

## items

Con el método `items` se obtiene una lista de tuplas (clave,valor).

```python
llaves.items()
```

## Ejercicio

Vamos a agregarle chimichurri al ejercicio anterior.

Ahora tenemos varios estudios: PnP, Assortman, Golden Stores, MMix, Sabines y como se llamen los productos que estén trabajando.

Cada producto tiene su equipo de consultores.

Los resultados vienen en alguna estructura de datos que ustedes quieran practicar.

Escribir un mail al equipo de consultores con los hallazgos y coso.