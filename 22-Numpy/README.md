# Numpy

## Arrays

Los `arrays` de `numpy` son arreglos `multidimensionales` del `mismo tipo`.

Para crear un array, se utiliza la función `array`.

Para crear un array de 0 dimensiones, se coloca el elemento dentro de la función.

```python
import numpy as np
escalar = np.array(5)
escalar
```

Para crear un vector, se pasa una lista con los elementos.

```python
vector = np.array([3,7,4])
vector
```

Para crear una matriz, se pasa una lista de "vectores" fila.

```python
matriz = np.array([[2,5,7],
                   [5,8,1]])
matriz
```

Se pueden crear arreglos n-dimensionales.

```python
cubo = np.array([
                 [[1,2],
                  [3,4]],
                 [[5,6],
                  [7,8]]
                ])
cubo
```

## shape

La propiedad shape nos va a indicar el tamaño de cada una de las dimensiones.

```python
print(escalar.shape)
print(vector.shape)
print(matriz.shape)
print(cubo.shape)
```

## Funciones del objeto vs Funciones de Numpy

En esta librería, los objetos van a tener funciones.

A su vez, las mismas funciones, en su mayoría están por fuera. Esto permite a los usuarios de otro lenguaje que no esté orientado a objetos a programar de una forma similar.

```python
print(cubo.sum())
np.sum(cubo)
```

## zeros

La función `zeros` genera un array de un tamaño determinado compuesto por `0`.

El argumento que toma esta función es una tupla con las dimensiones.

```python
np.zeros((4,7))
```

## ones

La función `ones` genera un array de un tamaño determinado compuesto por `1`.

```python
np.ones((3,4))
```

## arange

La función `arange` genera un array con una secuencia numérica.

Sus argumentos son `[límite inferior (inclusivo)]`, `límite superior (exclusivo)`, `[step]`.

```python
np.arange(10)
```

```python
np.arange(2,7)
```

```python
np.arange(-10,10,2)
```

## linspace

La función `linspace` genera un array con una secuencia de números equidistantes entre 2 números (incluidos los límites).

Los argumentos son `límite inferior`, `límite superior`, `cantidad de elementos`.

```python
np.linspace(3,7,20)
```

## random

### random.seed

Para establecer una semilla aleatoria y poder tener resultados reproducibles se utiliza la función `random.seed`. 

Esto garantiza que siempre que generemos números aleatorios tengamos el mismo resultado.

La semilla aleatoria no es algo trivial, es un reflejo de quiénes somos, así que piénsenla bien.

```python
np.random.seed(42)
```

### random.uniform

La función `random.uniform` genera un vector de números aleatorios distribuidos de forma uniforme entre 2 límites.

Los argumentos son `límite inferior`, `límite superior`, `[cantidad de elementos]`

```python
np.random.uniform(3,7,6)
```

### random.normal

La función `random.normal` genera un vector de números aleatorios distribuidos de forma normal.

Los argumentos son `media`, `desvío`, `[cantidad de elementos]`

```python
np.random.normal(5,2,7)
```

### random.choice

La función `random.choice` devuelve una muestra aleatoria de un array.

Por defecto es con reposición. En caso de querer hacerlo sin reposición, se debe colocar el argumento `replace=False`.

```python
np.random.choice(np.arange(1,20,.5),5)
```

### random.shuffle

La función `random.shuffle` reordena aleatoriamente el array que se le pase por argumento.

```python
a = np.arange(1,100,2.5)
np.random.shuffle(a)
a
```

>Usualmente `numpy` devuelve valores, en este caso modifica el objeto.

## Operaciones matemáticas

### Array con escalar

Al hacer una operación de un array con un escalar, se realiza una operacion llamada broadcasting.

Esto quiere decir, que se aplicará lo mismo a cada elemento del array.

```python
np.array([[1,2],
          [3,4],
          [5,6]])**2
```

Lo mismo sucede con operaciones lógicas por ejemplo.

```python
np.array([[1,2],
          [3,4],
          [5,6]])%2==0
```

### Array con Array (mismo tamaño)

Al hacer una operación entre 2 arrays, si los mismos poseen las mismas dimensiones, la operación se realizará elemento a elemento.

```python
a=np.array([[1,2],
            [3,4],
            [5,6]])
b=np.array([[11,12],
            [13,14],
            [15,16]])
a*b
```

### Array con Array (diferentes dimensiones)

En el caso de hacer una operación de diferentes dimensiones, lo que se requiere es que el tamaño de las dimensiones que poseen ambos sea el mismo.

```python
c = np.array([a,b])
c
```

```python
c+a
```

```python
a*np.array([2,1])
```

### Producto matricial

Para el producto matricial, se requiere que la cantidad de columnas del primer factor sea igual a la cantidad de filas del segundo factor.

Se puede escribir el caracter `@`.

```python
np.array([[1,2],[2,3]]) @ np.array([[5,6],[7,8]])
```

También se puede usar la función `dot`.

```python
np.array([[1,2],[2,3]]).dot(np.array([[5,6],[7,8]]))
```

## reshape

La función `reshape` devuelve un array redimensionado.

El argumento de la misma, son las nuevas dimensiones.

```python
np.array([1,2,3,4,5,6,7,8,9]).reshape((3,3))
```

## Sumarizaciones

Las mismas sumarizaciones que hay en `pandas` están en `numpy`.

```python
a.sum()
```

A la función de sumarización le puedo agregar el parámetro `axis=0` y voy a aplicar la función en el sentido de las columnas.

```python
a.max(axis=0)
```

Para aplicar una función en el sentido de las filas, el argumento es `axis=1`.

```python
a.min(axis=1)
```

## Funciones universales

`Numpy` viene con varias funciones que se aplicarán elemento a elemento.

Dada la implementación de `numpy`, esto es mucho más rápido que iterar.

```python
np.exp(a)
```

```python
np.sqrt(b)
```

```python
np.sin(c)
```

## Funciones vectoriales

La vectorización es uno de los elementos que más aumenta la performance en Python.

La función `vectorize` convierte una función que se aplica a un elemento único, en una función vectorial.

```python
def pasoASingular(palabra):
  if palabra[-1]=='s':
    return palabra[:-1]
  else:
    return palabra

pasoASingularVect = np.vectorize(pasoASingular)

pasoASingularVect(np.array(['holis','chauchis']))
```

## where

La función `where` puede utilizarse como el equivalente vectorial a la sintaxis `... if... else`.

Para ello sus argumentos son `condición`, `valor a devolver si se cumple`, `valor a devolver si no se cumple`.

```python
np.where(np.arange(10)%3==0,"buzz","fizz")
```

## T

La propiedad `T` devuelve el array traspuesto.

```python
print(matriz)
matriz.T
```

## vstack

La función `vstack` toma una lista de arrays y los coloca uno encima del otro.

Cabe destacar que las dimensiones tienen que ser compatibles

```python
np.vstack([np.array([1,2]),np.array([[3,4],[5,6]])])
```

## hstack

La función `hstack` toma una lista de arrays y los coloca uno al lado del otro.

Cabe destacar que las dimensiones tienen que ser compatibles

```python
np.hstack([np.array([1,2]).reshape((2,1)),np.array([[3,4],[5,6]])])
```

## Indexing

La forma de traer valores por medio de slicing cumple las mismas reglas que en el caso de las `Series`, sólo que ahora tendremos tantas posibilidades como dimensiones en nuestro array.

```python
slicear = np.arange(24).reshape((6,4))
```

Elemento único:

```python
slicear[2,3]
```

Varios elementos

```python
slicear[[2,3],[1,2]]
```

Slicing

```python
slicear[:-2,[1,2]]
```

Una columna entera

```python
slicear[:,2]
```

Una fila entera

```python
slicear[0,:]
```

Máscara Booleana

```python
slicear[:,np.array([True,False,True,False])]
```

## sort

La función `sort` devuelve el array ordenado.

```python
ordenar = np.array([13,  3, 16, 18, 17,  2, 17,  8, 12,  6])
np.sort(ordenar)
```

## argsort

La función `sort` devuelve los índices del array ordenado.

```python
np.argsort(ordenar)
```

## argmax

La función `argmax` devuelve el índice en el que está el máximo valor.

```python
np.argmax(ordenar)
```

## Relación con Pandas

Qué relación tienen los arrays de numpy con los Data Frames de Pandas?

Que son casi lo mismo.

Para extraer el array subyacente, se utiliza la propiedad `values` del DataFrame.

```python
import pandas as pd
iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
iris.head()
```

```python
iris.values
```

Como iris posee tipos de datos mixtos, todo el array va a ser de tipo `object`.

```python
iris.sepal_length.values
```

## nan

El objeto `nan` sirve para identificar un objeto Not a Number, en caso de querer introducir un nulo, se hace con este elemento.

```python
iris.iloc[0,4]=np.nan
iris.head()
```

## isin

Para evaluar si los elementos de un array están dentro de una lista, se utiliza la función `isin`.

```python
np.isin(cubo,[1,5])
```

## Documentación

Para la documentación de las rutinas, ver [https://numpy.org/devdocs/reference/routines.html](https://numpy.org/devdocs/reference/routines.html)