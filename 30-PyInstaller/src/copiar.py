import os
from shutil import copyfile

files = ['unacarpeta.py','unarchivo.py','config.json']

for f in files:
    copyfile(f,os.path.join('..','user',f))

folder = ['auxiliar','data']

for f in folder:
    if not os.path.exists(os.path.join('..','user',f)):
        os.mkdir(os.path.join('..','user',f))

if not os.path.exists(os.path.join('..','user','data','input')):
    os.mkdir(os.path.join('..','user','data','input'))

copyfile(os.path.join('auxiliar','functions.py'),os.path.join('..','user','auxiliar','functions.py'))

files = ['iris.csv','treemap argentina.xlsx']

for f in files:
    copyfile(os.path.join('data','input',f),os.path.join('..','user','data','input',f))