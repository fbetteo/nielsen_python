import numpy.random.common
import numpy.random.bounded_integers
import numpy.random.entropy
import pandas as pd
import json
import os
from auxiliar.functions import checkReadAndDescribe

os.chdir(os.path.dirname(os.path.abspath(__file__)))

with open('config.json','rt') as f:
    config = json.loads(f.read())

lista_archivos = os.listdir(config['input'])
lista_archivos.sort()

for archivo in lista_archivos:
    checkReadAndDescribe(os.path.join(config['input'],archivo))