import os
import pandas as pd

def checkReadAndDescribe(path):
    if path.lower().endswith('xlsx') or path.lower().endswith('xls'):
        df = pd.read_excel(path)
    else:
        df = pd.read_csv(path)
    print("Resumen de {}".format(os.path.split(path)[-1]))
    print(df.describe())