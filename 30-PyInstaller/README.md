# PyInstaller

`PyInstaller` es una librería que genera un _bundle_ con los archivos y dependencias que necesitemos para ejecutar un programa.

La documentación se encuentra en [https://pyinstaller.readthedocs.io/](https://pyinstaller.readthedocs.io/)

## Instalación

En nuestro environment ejecutar:

```bash
pip install pyinstaller
```

## Archivo único

Para generar un archivo único tenemos que escribir en consola en nuestro environment `pyinstaller -F archivo`

```bash
pyinstaller -F unarchivo.py
```

Lo que va a hacer es:

* Crear un archivo `unarchivo.spec` en la misma carpeta del script.
* Crea una carpeta `build` si no existe
* Escribe logs y algunos archivos que necesita en la carpeta `build`
* Crea la carpeta `dist` si no existe
* Crea el ejecutable en la carpeta `dist`

En la carpeta `dist` se encuentra el programa a compartir con los usuarios finales.

## Carpeta

Si quiero generar una carpeta, elimino la opción `-F`.

## Agregando archivos y carpetas al bundle usando .spec

Puedo agregar archivos en el momendo de la ejecución usando la opción `--add-data`, sin embargo, en el caso de haber muchos archivos, esto puede ser algo engorroso.

Para ello, se debe ejecutar `pyi-makespec [opciones] script` lo cual nos va a generar el archivo `.spec` pero no va a crear el bundle.

Este archivo posee el siguiente formato:

```python
# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['unacarpeta.py'],
             pathex=['/home/pogo/Repositorios/nielsen_python/30-PyInstaller/user'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='unacarpeta',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='unacarpeta')
```

Para incluir carpetas y archivos, tenemos que pasar en el argumento `datas` una lista con tuplas del formato `(ubicación de archivo original,ubicación en el bundle)`.

Ejemplo:

```python
########################
datas=[
       ('data','data'),
       ('config.json','.')
      ],
########################
```

Y luego ejecutar `pyinstaller script.spec`

```bash
pyinstaller unacarpeta.spec
```