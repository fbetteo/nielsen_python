# Intro a Git

## Qué es?

`Git` es un sistema de control de versiones que permite manejar proyectos de desarrollo.

## Cómo se usa?

El siguiente gráfico detalla el flujo de trabajo que implica utilizar `Git` como sistema de reversionado:

![git](img/git.png)

* **Working directory (directorio de trabajo):** es la carpeta raíz/principal donde se albergan todos los archivos y sub-directorios que componen el proyecto.
* **Staging area (área de preparación):** es un índice en donde se detallan los archivos creados o modificados en el proyecto.
* **Local repository (repositorio local):** es el espacio local en donde queda almacenada una copia de cada uno de los archivos manipulados, junto con el historial de los cambios realizados.
* **Remote repository (repositorio remoto):** es el espacio remoto en un servidor, en donde se guarda una copia del repositorio local (donde estan los archivos manipulados y los cambios realizados).

## Nuestro caso

Nosotros vamos a usar `Git` sólo para el material, así que no vamos a andar con los add y los commit y los push sino que vamos a usar sólo 2 comandos:

### Clone

El comando ```git clone``` clona un repositorio remoto en una carpeta local.

Para eso, en la línea de comandos posicionarse en una carpeta y escribir:

```bash
git clone https://gitlab.com/lucpogo/nielsen_python.git
```

Va a crear la carpeta `nielsen_python` dentro de la cual va a estar el contenido del repositorio (o repo si se quieren hacer los cool, developers cancheros en Twitter).

### Pull

El comando ```git pull``` es el que va a actualizar nuestro repositorio local con las actualizaciones del repositorio remoto.

Para eso, en la carpeta `nielsen_python` hay que escribir el comando:

```bash
git pull
```

Hay que tener en cuenta que las actualizaciones se hacen con respecto a la rama de desarrollo que poseía previamente:

```
                  Cambian algo en el remoto
                 /                           \
                /                             \
Venía pulleando                                \ Trambólico!!! Hay que Saber
como un campeón                                / Pullear y Pushear
                \                             /
                 \                           /
                  Yo cambio algo en el local                                 
```

Por lo tanto, para los fines de este curso, cada capítulo tiene una carpeta `user` donde puede copiar el código y probar sin romper nada.
