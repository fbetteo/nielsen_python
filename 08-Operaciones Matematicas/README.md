# Operaciones Matemáticas

## Operadores

* **Suma:** +
* **Resta:** -
* **Multiplicación:** *
* **División:** /
* **Potencia:** **
* **División entera:** //
* **Módulo (resto):** %

## Paréntesis

Los paréntesis tienen el mismo sentido que en aritmética tradicional.

## Precedencia

Las operaciones se irán resolviendo utilizando el criterio `PEMDAS` (Parentheses, Exponents, Multiplcation and  Division, Add and Substract) y de izquierda a derecha.

Ejemplo:

```python
7/4*2+8*(3-5**2)-2
```

Lo primero que va a resolver es `(3-5**2)` y dentro de esos paréntesis, primero resuelve 5**2=25 y después 3-25=-22.

Lo siguiente es 7/4=1.75 y 8\*(-22)=-176

Después 1.75*2=3.5

Después 3.5 - 176 = -172.5

Finalmente -172.5-2=174.5
