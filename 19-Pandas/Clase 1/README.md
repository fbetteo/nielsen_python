# Pandas - Clase 1

Pandas es una librería de Python que nos permite trabajar con estructuras de datos tabulares llamadas `DataFrames`.

Posee una documentación bastante completa, a la que se puede acceder en [https://pandas.pydata.org/pandas-docs/stable/](https://pandas.pydata.org/pandas-docs/stable/)

## Librería

Por convención la librería se importa con el alias `pd`.

```python
import pandas as pd
pd.__version__
```

## Lectura de archivos

La función principal para importar un archivo es `read_csv`.

```python
import os
iris = pd.read_csv(os.path.join('data','iris.csv'))
```

Algunas opciones y sus valores por defecto:

* __sep:__ El string de campos, por defecto es _,_.
+ __header:__ Qué línea posee los nombres de las columnas. Por defecto va a inferir si la primera fila posee los nombres de las columnas o no.
* __name__: Una lista con lso nombres de las columnas.
* __index_col:__ La columna del archivo que va a ser usada para ponerle nombre a las filas.
* __na_values:__ El string con el cual se escriben los valores faltantes en el archivo de entrada.
* __skiprows:__ Cantidad de filas a saltear.
* __nrows:__ Cantidad de filas a leer. Combinado con skip rows, es práctico para leer archivos grandes.
* __encoding:__ Para identificar el encoding del archivo.

```python
mtcars = pd.read_csv(os.path.join('data','mtcars.csv'),
                     sep=';',
                     index_col=0)
```

## Series

Cada DataFrame está compuesto por elementos de tipo `Series`. Estos son vectores que poseen un label y un tipo de dato.

Los mismos pueden ser llamados usando `dot notation`. En ese caso, para trabajar con la columna vamos a escribir `dataframe.columna` en este caso, el nombre de la columna debe cumplir las reglas de las variables de Python.

```python
iris.Species
```

La otra opción es `bracket notation`. En este caso, se escribe el nombre de la columna entre corchetes y el mismo debe ser un string.

```python
iris['Petal.Width']
```

### Operaciones vectoriales

Las Series permiten realizar operaciones de forma vectorizada, esto quiere decir que tomando 2 series, una operación entre ellas se realizará elemento a elemento y va a ser mucho más eficiente que un loop.

```python
iris['Petal.Length']*iris['Petal.Width']
```

## shape

La propiedad `shape` es una tupla con la cantidad de filas y columnas que posee el DataFrame.

```python
iris.shape
```

## columns

La propiedad `colummns` hace referencia a las columnas, las misma sirve tanto para ver las columnas como para modificarla.

```python
iris.columns
```

## index

La propiedad `index` nos permite ver el índice.

```python
mtcars.index
```

## dtypes

La propiedad `dtypes` nos indicará el tipo de datos de cada columna.

```python
iris.dtypes
```

## head

El método head nos devolverá un DataFrame con las primeras `n` filas (por defecto n=5).

```python
iris.head(10)
```

## tail

El método head nos devolverá un DataFrame con las últimas `n` filas (por defecto n=5).

```python
iris.tail()
```

## info

El método `info` nos resume rápidamente el contenido, sin entrar en detalles de las variables.

```python
iris.info()
```

### describe

El método `describe` nos da información sobre la distribución de los datos.

```python
mtcars.describe()
```

Por defecto nos traerá información de las variables numéricas, para traer otro tipo de dato, tenemos que pasar el parámetro `include` y qué tipo de datos analizar.

```python
iris.describe(include=['float','object'])
```

## Selección de Casos

### Slicing

Como en las listas y en las tuplas, las Series cumplen con casi las mismas reglas que las listas.

```python
iris['Sepal.Length'][2:5]
```

```python
iris['Sepal.Length'][3]
```

>No funcionan los índices negativos para seleccionar elementos individuales, pero sí en el Slicing.

También puedo seleccionar varios elementos individuales pasando una lista entre los corchetes.

```python
iris['Sepal.Length'][[3,57,90]]
```

### Máscara booleana

Se puede seleccionar casos del DataFrame que cumplan determinada condición utilizando lo que se llama `máscara booleana`. Esto es un vector de booleans.

```python
mascara = iris.Species=='setosa'
mascara
```

```python
iris[mascara]
```

Si bien nosotros almacenamos la máscara en una variables, es posible no hacerlo.

```python
iris[iris.Species=='versicolor']
```

Con el caracter `~` se niega la condición.

```python
iris[~(iris.Species=='setosa')]
```

Con el caracter `&` se evalúa que se cumplan 2 condiciones.

```python
iris[(iris.Species=='setosa') & (iris['Sepal.Width']<3)]
```

Con el caracter `|` se evalúa que se cumpla al menos una condición.

```python
iris[(iris.Species=='setosa') | (iris['Sepal.Width']<3)]
```

### Seleccionar más de 1 variable

Para selecccionar más de 1 variable, se coloca una lista con los nombres de las variables a seleccionar.

```python
iris[['Sepal.Length','Species']]
```

### Excluir de la selección

Para excluir una observación, se utiliza el método drop.

Para excluir columnas, el argumento es `columns=[columnas a excluir]`.

```python
iris.drop(columns=['Sepal.Length','Sepal.Width'])
```

Para excluir observaciones debemos especificarlas con el argumento index

```python
mtcars.drop(index=['Mazda RX4','Mazda RX4 Wag'])
```

### loc

Para poder utilizar la sintaxis de selección [filas,columnas] tenemos que utilizar la propiedad `.loc` y luego seleccionar filas y columnas por nombre.

```python
mtcars.loc[['Merc 240D', 'Honda Civic','Honda Civic'],['disp','wt']]
```

Si quiero seleccionar todas las columnas, tengo que poner `:` en el segundo campo.

```python
mtcars.loc[['Merc 280', 'Merc 280C', 'Merc 450SE', 'Merc 450SL', 'Merc 450SLC'],:]
```

Las reglas del slicing se aplican, pero el límite final es `inclusivo`.

```python
mtcars.loc['Merc 240D':'Merc 450SLC','vs':'drat':-1]
```

### iloc

Para seleccionar filas y columnas por posición, utilizo la propiedad `.iloc`.

```python
iris.iloc[2:6,[0,-1]]
```

### funciones lambda

Las funciones lambda son funciones anónimas que se crean y ejecutan en el momento.

Son útiles para trabajar después de haber manipulado un DataFrame.

La sintaxis es lambda <elemento sobre el cual aplicar la función>:<acción sobre el elemento>.

```python
iris[iris.Species=='setosa'].loc[lambda row:row['Sepal.Length']*row['Sepal.Width']<15]
```

En este caso, se va a aplicar la función a la fila y se va a trabajar la misma como si fuese un diccionario.

## Ordenando

### sort_index

El método `sort_index` permite ordenar por índice.

```python
mtcars.sort_index()
```

Para ordenar descendentemente se utiliza el argumento `ascending=False`.

```python
mtcars.sort_index(ascending=False)
```

### sort_values

Para ordenar por valor, se utiliza el método `sort_values` que lleva como argumento una lista con las columnas por las cuales ordenar.

```python
mtcars.sort_values('mpg')
```

## Agregar o modificar una columna

### Brackets

Puedo agregar o modificar una columna con la sintaxis `DataFrame[columna]`.

```python
romper = iris.copy()
romper['Petal.Square.Area'] = romper['Petal.Length']*romper['Petal.Width']
romper.head()
```

En este caso se modifica el DataFrame.

### assign

El método `assign` agrega una variable sin modificar el DataFrame original.

```python
romper.assign(sepal_petal_ratio=lambda row:row['Petal.Length']/row['Sepal.Length'])
```

### modificando vía loc

Así como podemos seleccionar casos, también podemos dar valor a las observaciones de cierto índice.

```python
romper.iloc[:10,4]='Holiiiiis'
romper.head(20)
```

## Ejercicio

Importar el archivo el archivo Vb_semanalizadas.csv de la carpeta data y analizarlo su estrictura usando la sintaxis de VSCode.

Para ver cómo escribir en markdown, ver [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

Luego, hacer las siguientes manipulaciones:

* Filtros simples
* Filtrar por día del mes (usar lambda)
* Ordenar por value descendente
* Crear una columna que sea value/ValueSplit para los casos que value no sea 0
