# Pandas - Clase 2

## Modificando el tipo de datos

### astype

El método `astype` permite modificar el tipo de dato de una Series.

```python
import pandas as pd
import os
tae = pd.read_csv(os.path.join('data','tae.data'))
tae.dtypes
```

El dataset tae contiene datos de Teacher Assistant y los tipos de datos subyacentes son:

1. Whether of not the TA is a native English speaker (binary)
      1=English speaker, 2=non-English speaker
2. Course instructor (categorical, 25 categories)
3. Course (categorical, 26 categories)
4. Summer or regular semester (binary) 1=Summer, 2=Regular
5. Class size (numerical)
6. Class attribute (categorical) 1=Low, 2=Medium, 3=High

Como vemos, las variables `Course Instructor`, `Course` y `Class attribute` son variables categóricas con una cantidad de atributos.

En Pandas este tipo de variables se denominan `category` y se distinguen de los strings (object) en que las primeras pueden tomar un valor determinado de antemano mientras las segundas pueden tomar cualquier valor.

Las mismas pueden estar ordenadas o no.

Pasamos Course Instructor y Course a category no ordenada con astype:

```python
tae['Course Instructor']=tae['Course Instructor'].astype('category')
tae['Course']=tae['Course'].astype('category')
tae.dtypes
```

### Categorical

La función `Categorical` de Pandas permite crear un tipo de dato categórico pero nos da más flexibilidad al crearlo, ya que podemos establecer los valores posibles que puede tomar la variable y además si están ordenadas (ordinales) o no.

```python
tae['Class Attribute']=pd.Categorical(tae['Class Attribute'],
                                      categories=[1,2,3],
                                      ordered=True)
```

### to_datetime

La función `to_datetime` devuelve una Series con tipo de dato `datetime`. Para eso, hay que especificar el formato en el cual viene la fecha.

Para ver los formatos, se puede ir a [https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior)


```python
dji=pd.read_csv(os.path.join('data','dow_jones_index.data'))
print(dji.head())
dji.info()
```

Vemos que la variable `date` posee dtype object y que está en formato `m/d/yyyy`.

```python
dji['date']=pd.to_datetime(dji['date'],format='%m/%d/%Y')
dji['date'].head()
```

## select_dtypes

El métdodo `select_dtypes` nos permite seleccionar las columnas que posea un tipo de dato determinado.

Los argumentos son `include=[lista de tipos de datos a incluir]` o `exclude=[lista de tipos de datos a excluir]`.

* Para seleccionar numéricos: 'number'
* Para seleccionar strings: 'object'

```python
iris = pd.read_csv(os.path.join('data','iris.csv'))
iris.select_dtypes(include='number')
```

## apply

El método apply sirve para ejecutar una función por columnas o por filas.

Si no especificamos un eje, va a hacerlo en el sentido de las columnas

```python
iris.iloc[:,:-1].apply(lambda col:sum(col**2))
```

Si lo queremos hacer en el sentido de las filas, debemos especificar `axis=1`.

```python
tae['Summer or Regular']=tae.apply(lambda row:'summer' if row['Summer or Regular']==1 else 'REGULAR',axis=1)
tae.head()
```

También podemos utilizarlo en una Series.

```python
tae['Native English Speaker']=tae['Native English Speaker'].apply(lambda x:'YES' if x==1 else 'NO')
```
## rename

El método `rename` sirve para renombrar índices o columnas. 

Para ello se debe pasar el argumento `columns` un diccionario con el formato `{'nombre viejo':'nombre nuevo'}`

```python
tae = tae.rename(columns={'Summer or Regular':'Season'})
tae.info()
```

## str

La propiedad `str` permite trabajar con una Series de tipo object y manipular su contenido como a cualquier string, por lo cual se pueden aplicar los métodos vistos en [Strings](../../09-Strings).

```python
tae['Season']=tae['Season'].str.upper()
tae['Season']
```

## Missings

### isnull

El método `isnull` de una Series me va a devolver True si es un valor nulo y False en caso contrario. Esto lo puedo usar como máscara booleana.

```python
dji[dji.percent_change_volume_over_last_wk.isnull()].iloc[:,5:10]
```

### fillna

El método `fillna` reemplaza los nulos por un valor que uno diga.

```python
dji[dji.percent_change_volume_over_last_wk.isnull()].fillna(-99999)
```

### dropna

El método `dropna` nos devuelve un DataFrame sin las observaciones que poseen un valor nulo.

```python
dji.dropna().head()
```

## Estadísticas descriptivas

Puedo calcular varias estadísticas descriptivas aplicando el método a una Serie o a un DataFrame.

Las estadísticas disponibles son:

|Variable|Descripción|
|----|----|
|count|	Cantidad de Valores no Nulos|
|sum|	Suma|
|mean|	Media|
|mad|	Desvío absoluto medio|
|median|	Mediana|
|min|	Mínimo|
|max|	Máximo|
|mode|	Moda|
|abs|	Valor Absoluto|
|prod|	Productoria|
|std|	Desvío Standard (insesgado)|
|var|	Varianza (insesgada)|
|sem|	Error standard de la media|
|skew|	Asimetría|
|kurt|	Kurtosis|
|quantile|	Cuantiles|
|cumsum|	Suma acumulativa|
|cumprod|	Producto acumulativo|
|cummax|	Máximo acumulativo|
|cummin|	Mínimo acumulativo|

Al aplicarlas a un DataFrame, voy a obtener la medida para cada variable. El resultado va a ser una Series.

```python
mtcars = pd.read_csv(os.path.join('data','mtcars.csv'),sep=';', index_col=0)
mtcars.mean()
```

Si quiero aplicar la medida en el sentido de las filas, tengo que utilizar el parámetro `axis=1`

```python
iris.select_dtypes('number').std(axis=1)
```

## groupby

El método `groupby` va a dividir el dataset por una o más variables del mismo para luego aplicar algún método.

Los argumentos que va a recibir el método es una lista con los nombres de las columnas por las cuales vamos a agrupar.

Lo que devuelva va a depender del método y los valores de las variables de agrupación se convertirán en el índice del resultado.

### size

El método size me va a devolver el tamaño del grupo.

```python
mtcars.groupby(['cyl','am']).size()
```

### Estadísticas descriptivas

Se pueden usar cualquiera de las estadísticas descriptivas vistas en el anterior. 

Lo que va a devolver, es la estadística para cada variable.

```python
mtcars.groupby('am').mean()
```

Como lo que devolvió es un DataFrame, si quiero la medida de una variable en particular puedo filtrarlo antes:

```python
mtcars[['am','mpg']].groupby('am').mean()
```

Filtrarlo luego del groupby:

```python
mtcars.groupby('am')['mpg'].mean()
```

O bien filtrarlo al final

```python
mtcars.groupby('am').mean()['mpg']
```

>Cabe destacar que el primer caso devuelve siempre un DataFrame y los otros van a devolver de acuerdo a la cantidad de variables pedidas.

### aggregate o agg

Para tener más control sobre las sumarizaciones, vamos a utilizar el método `agg` (o aggregate, depende de qué tanto más quieras escribir).

A partir de la versión 0.25 de Pandas, los desarrolladores se pusieron finalmente las pilas e implementaron la función de pandas `NamedAgg` que se va a usar de la siguiente forma: `nombre_del_resultado=pd.NamedAgg(column='columna que voy a sumarizar,aggfunc='funcion de sumarización')`

```python
dji.groupby('stock').agg(
    average_volume=pd.NamedAgg(column='volume', aggfunc='mean'),
    minimum_volume=pd.NamedAgg(column='volume', aggfunc='min'),
    maximum_volume=pd.NamedAgg(column='volume', aggfunc='max'),
    maximum_percent_change_price=pd.NamedAgg(column='percent_change_price', aggfunc='max')
)
```

Además de las funciones por defecto, podemos agregar funciones nuestras.

```python
def rango(serie):
    return max(serie)-min(serie)

dji.groupby('stock').agg(
    average_volume=pd.NamedAgg(column='volume', aggfunc='mean'),
    minimum_volume=pd.NamedAgg(column='volume', aggfunc='min'),
    maximum_volume=pd.NamedAgg(column='volume', aggfunc='max'),
    maximum_percent_change_price=pd.NamedAgg(column='percent_change_price', aggfunc='max'),
    volume_percent_change_price=pd.NamedAgg(column='percent_change_price', aggfunc=rango)
)
```

#### Sintaxis anterior

Antes de la versión 0.25 de Pandas, había que pasar un diccionario con las variables a sumarizar y las funciones.

```python
dji.groupby('stock').agg(
    {'volume':['mean','min','max'],
     'percent_change_price':['max',rango]}
)
```

Lo que devolvía era un DataFrame con MultiIndex en las columnas, lo que se puede decir una total, y absoluta reverendísima.

## reset_index

Pandas es una de las librerías con la peor sintaxis del mundo, estamos de acuerdo no?

Una de las cosas que hacen que pandas sea insoportable es que al hacer la agrupación nos ponga las varables en índice. Y si yo las quería usar? Qué? No puedo usarlas como a mí se me antoje? Son MIS variables Python, MÍAS!!!!

Por suerte está el método `reset_index` que lo que va a hacer es volar el índice y dejarme usarlo como variable.

```python
mtcars.groupby(['cyl','am']).size().reset_index()
```

Cuando lo aplicamos a una Series, el nombre de la misma se pierde, por lo cual, podemos pasar el argumento `name='nombre nuevo'` al método y nos devuelve el nombre que nosotros le indiquemos.

```python
mtcars.groupby(['cyl','am']).size().reset_index(name='cantidad')
```

## Ejercicio

Levantar un archivo del producto para el que trabajamos, procesarlo para que tenga el formato necesario y hacerle sumarizaciones.