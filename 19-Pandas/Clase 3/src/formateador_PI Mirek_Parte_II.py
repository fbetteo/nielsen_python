print(
'''
PRICING INSIGHTS --- Final Delivery Tool

****************************************
****************************************
''')


# Import libraries
import os
import csv
import pandas as pd
import numpy as np

import warnings
warnings.filterwarnings("ignore")

# Retrieve current working directory
cwd = os.getcwd()
print('-- Retrieving current working directory: {}'.format(cwd))


currency = input('-- What is the currency for this project?: ')






# Read files Input File
file_name = input('-- Please, enter the input file name (csv file): ')
df = pd.read_csv(file_name, encoding=r'latin_1', header='infer', index_col=None , low_memory=False)



#######################################
############ LOAD SUMMARIES ###########
#######################################

summaries_input = input('-- Do you want to calculate summaries? (y/n): ')

if summaries_input == 'y':
	'''
	Weight Average Functions
	'''
	price_strat_reg = input('\t-- Please, enter the Regular Price Strategy parameter: ')
	price_strat_promo = input('\t-- Please, enter the Promoted Price Strategy parameter: ')

	def w_average_value_market(series):
		dropped = series.dropna()
		dropped = dropped[dropped != 0]
		try:
			return np.average(dropped, weights=df.loc[dropped.index, 'total_value_market'])
		except:
			return np.average(dropped)

	def w_average_value_cli(series):
		dropped = series.dropna()
		dropped = dropped[dropped != 0]
		try:
			return np.average(dropped, weights=df.loc[dropped.index, 'total_value_cli'])
		except:
			return np.average(dropped)

	def w_average_value_rem(series):
		dropped = series.dropna()
		dropped = dropped[dropped != 0]
		try:
			return np.average(dropped, weights=df.loc[dropped.index, 'total_value_rem'])
		except:
			return np.average(dropped)

	def w_average_units_market(series):
	    dropped = series[series != 0].dropna()
	    try:
	    	return np.average(dropped, weights=df.loc[dropped.index, 'total_units_market'])
	    except:
	    	return np.average(dropped)

	def w_average_units_cli(series):
	    dropped = series[series != 0].dropna()
	    try:
	    	return np.average(dropped, weights=df.loc[dropped.index, 'total_units_cli'])
	    except:
	    	return np.average(dropped)

	def w_average_units_rem(series):
	    dropped = series[series != 0].dropna()
	    try:
	    	return np.average(dropped, weights=df.loc[dropped.index, 'total_units_rem'])
	    except:
	    	return np.average(dropped)

	###############################
	###							###
	###	 	Decile Summary  	###
	###							###
	###############################
	
	deciles_agg_funcs = {
		'PI_regular' : w_average_value_market,		
		'PI_promo' 	: w_average_value_market,
		'UPC Tag' : 'count',
		'total_value_market' : 'sum',
		'total_value_cli' : 'sum',
		'total_value_rem' : 'sum',
	}
	
	sum_dec = df.groupby(['Trading Area Name' , 'Market decile']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	###############################
	###							###
	###	 	Decile/Cat Summary  ###
	###							###
	###############################
	sum_dec_cat = df.groupby(['Trading Area Name' , 'Market decile' , 'Category Name']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	
	###############################
	###							###
	###	 Regular Boxes Summary  ###
	###							###
	###############################
	sum_reg_box = df.groupby(['Trading Area Name' , 'box_regular']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	###############################
	###							###
	###	 Promo Boxes Summary    ###
	###							###
	###############################
	sum_promo_box = df.groupby(['Trading Area Name' , 'box_promo']).agg(deciles_agg_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	#####################################
	###								  ###
	###	 Regular/Cat Boxes Summary    ###
	###							      ###
	#####################################
	cat_boxes_funcs = {
		'PI_regular' : 'mean',		
		'PI_promo' 	: 'mean',
		'UPC Tag' : 'count',
		'total_value_market' : 'sum',
		'total_value_cli' : 'sum',
		'total_value_rem' : 'sum',
	}
	sum_reg_box_cat = df.groupby(['Trading Area Name' , 'box_regular' , 'Category Name' ]).agg(cat_boxes_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	#####################################
	###								  ###
	###	 Promo/Cat Boxes Summary      ###
	###							      ###
	#####################################
	sum_promo_box_cat = df.groupby(['Trading Area Name' , 'Category Name' , 'box_promo']).agg(cat_boxes_funcs).reset_index()\
						.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
										'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining'})
	###############################
	###							###
	###	 	Category Summary    ###
	###							###
	###############################
	category_agg_funcs = {
		'Client Regular Elasticity' : w_average_units_cli,
		'Remaining Regular Elasticity' : w_average_units_rem,
		'Client Promo Elasticity' : w_average_units_cli,
		'Remaining Promo Elasticity' : w_average_units_rem,
		'PI_regular' : w_average_value_market,		
		'PI_promo' 	: w_average_value_market,
		'Discount Frequency_cli' : w_average_value_cli,
		'Discount Frequency_rem' : w_average_value_rem,
		'Discount Depth_cli' : w_average_value_cli,
		'Discount Depth_rem' : w_average_value_rem,
		'UPC Tag' : 'count',
		'total_value_market' : 'sum',
		'total_value_cli' : 'sum',
		'total_value_rem' : 'sum',
		'total_value_promo' : 'sum',
		'total_value_reg' : 'sum',
		'total_value_promoted_cli' : 'sum',
		'total_value_promoted_rem' : 'sum',
	}


	sum_cat = df.groupby(['Trading Area Name' , 'Category Name']).agg(category_agg_funcs).reset_index()\

	sum_cat['Price Strategy Client'] = 'Otro'
	sum_cat['Price Strategy Remaining'] = 'Otro'
	sum_cat.loc[(sum_cat['Client Regular Elasticity'] <= float(price_strat_reg)) & \
				(sum_cat['Client Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Client'] = 'Price Fighter'

	sum_cat.loc[(sum_cat['Client Regular Elasticity'] >= float(price_strat_reg)) & \
				(sum_cat['Client Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Client'] = 'Hi-No'

	sum_cat.loc[(sum_cat['Client Regular Elasticity'] <= float(price_strat_reg)) &	\
				(sum_cat['Client Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Client'] = 'EDLP'

	sum_cat.loc[(sum_cat['Client Regular Elasticity'] >= float(price_strat_reg)) &	\
				(sum_cat['Client Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Client'] = 'Hi-Low'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] <= float(price_strat_reg)) & \
				(sum_cat['Remaining Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'Price Fighter'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] >= float(price_strat_reg)) & \
				(sum_cat['Remaining Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'Hi-No'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] <= float(price_strat_reg)) &	\
				(sum_cat['Remaining Promo Elasticity'] >= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'EDLP'

	sum_cat.loc[(sum_cat['Remaining Regular Elasticity'] >= float(price_strat_reg)) &	\
				(sum_cat['Remaining Promo Elasticity'] <= float(price_strat_promo)) , 'Price Strategy Remaining'] = 'Hi-Low'

	sum_cat['Promo %'] = sum_cat['total_value_promo'] / sum_cat['total_value_market']
	sum_cat['Promo Client %'] = sum_cat['total_value_promoted_cli'] / sum_cat['total_value_cli']
	sum_cat['Promo Remaining %'] = sum_cat['total_value_promoted_rem'] / sum_cat['total_value_rem']

	sum_cat = sum_cat.rename(columns={'UPC Tag' : 'SKU Counts' , 'total_value_market' : 'Total Value Market' ,
							'total_value_cli' : 'Total Value Client' , 'total_value_rem' : 'Total Value Remaining',
							'total_value_promo': 'Total Promoted Value' , 'total_value_reg' : 'Total Regular Value' ,
							'total_value_promoted_cli' : 'Total Client Promoted Value' , 'total_value_promoted_rem' : 'Total Remaining Promoted Value',
						})
	

else:
	sum_dec = pd.read_csv('summary_deciles.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_dec_cat = pd.read_csv('summary_deciles_cat.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_cat = pd.read_csv('summary_category.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_reg_box = pd.read_csv('summary_boxes_regular.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_reg_box_cat = pd.read_csv('summary_boxes_regular_cat.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_promo_box = pd.read_csv('summary_boxes_promo.csv', encoding=r'latin_1', header='infer', index_col=None)
	sum_promo_box_cat = pd.read_csv('summary_boxes_promo_cat.csv', encoding=r'latin_1', header='infer', index_col=None)	




print('')
print('In process: Data formatting...')
print('')

# Define main 
main = pd.DataFrame()


# Pick 10% of df to ease computation
#df = df.sample(frac=0.1, random_state=1)



# Create 'General Information'
main['UPC Tag'] 	= df['UPC Tag']
main['Barcode'] 	= df['Barcode']
main['Description'] = df['Description']
main['Brand'] 		= df['Brand']
main['Trading Area Name'] = df['Trading Area Name']
main['Category Number'] = df['Category Number']
main['Category Name'] = df['Category Name'] 

# Create 'Scope Info'
main['Summary'] = df['Summary']
main['Market decile'] = df['Market decile']

# Create 'Price Indexes' CON INPUT MEDIAN - MEAN - MODE QUE FIGURE EN EL NOMBRE DEL FILE FINAL
main['PI_regular'] = df['PI_regular']
main['PI_promo']   = df['PI_promo']


# Create 'Elasticities'
main['Client Regular Elasticity']		= df['Client Regular Elasticity']
main['Client Promo Elasticity'] 		= df['Client Promo Elasticity']
main['Remaining Regular Elasticity'] 	= df['Remaining Regular Elasticity']
main['Remaining Promo Elasticity'] 		= df['Remaining Promo Elasticity']



# Create 'Elasticity Range' 
main['range_rpe_client'] 	= df['range_rpe_client']
main['range_rpe_rem'] 		= df['range_rpe_rem']
main['range_ppi_client'] 	= df['range_ppi_client']
main['range_ppi_rem'] 		= df['range_ppi_rem']



# Create 'Boxes'
main['box_regular'] 	= df['box_regular']
main['box_regular_cod'] = df['box_regular_cod']
main['box_promo'] 		= df['box_promo']
main['box_promo_cod'] 	= df['box_promo_cod']



# Create 'Price Strategy'
main['price_strategy_client'] = df['price_strategy_client'] 
main['price_strategy_rem'] = df['price_strategy_rem']
main['KVI Client Strategy'] = df['KVI Client Strategy']




# Create 'Average Prices'
main['AvgPrice_nonpromoted_cli'] 	= df['AvgPrice_nonpromoted_cli'] 
main['AvgPrice_nonpromoted_rem'] 	=  df['AvgPrice_nonpromoted_rem']
main['AvgPrice_promoted_cli'] 		=  df['AvgPrice_promoted_cli']
main['AvgPrice_promoted_rem'] 		=  df['AvgPrice_promoted_rem']




# Create 'Discount Info'
main['Discount Depth_cli'] 		= df['Discount Depth_cli']
main['Discount Depth_rem'] 		= df['Discount Depth_rem']
main['Discount Frequency_cli'] 	= df['Discount Frequency_cli']
main['Discount Frequency_rem'] 	= df['Discount Frequency_rem']



# Create 'Sales Information'
main['total_value_nonpromoted_cli'] 	=  df['total_value_nonpromoted_cli']
main['total_value_nonpromoted_rem'] 	=  df['total_value_nonpromoted_rem'] 
main['total_value_promoted_cli'] 		= df['total_value_promoted_cli']
main['total_value_promoted_rem'] 		= df['total_value_promoted_rem']

main['total_units_nonpromoted_cli'] 	= df['total_units_nonpromoted_cli']
main['total_units_nonpromoted_rem'] 	= df['total_units_nonpromoted_rem']
main['total_units_promoted_cli'] 		= df['total_units_promoted_cli']
main['total_units_promoted_rem'] 		= df['total_units_promoted_rem']

main['total_value_cli'] 	= df['total_value_cli']
main['total_value_rem'] 	= df['total_value_rem']
main['total_value_market'] 	= df['total_value_market']
main['total_value_reg'] 	=  df['total_value_reg']
main['total_value_promo'] 	= df['total_value_promo']

main['total_units_cli'] 	= df['total_units_cli']
main['total_units_rem'] 	= df['total_units_rem']
main['total_units_market'] 	= df['total_units_market']
main['total_units_reg'] 	= df['total_units_reg']
main['total_units_promo'] 	= df['total_units_promo']

# Create 'Percentiles/Most Common Prices'
main['Percentile_2_nonpromoted_cli'] = df['Percentile_2_nonpromoted_cli']
main['Percentile_5_nonpromoted_cli'] = df['Percentile_5_nonpromoted_cli']
main['Percentile_10_nonpromoted_cli'] = df['Percentile_10_nonpromoted_cli']
main['Percentile_15_nonpromoted_cli'] = df['Percentile_15_nonpromoted_cli']
main['Percentile_20_nonpromoted_cli'] = df['Percentile_20_nonpromoted_cli'] 
main['Percentile_25_nonpromoted_cli'] = df['Percentile_25_nonpromoted_cli']
main['Percentile_30_nonpromoted_cli'] = df['Percentile_30_nonpromoted_cli']
main['Percentile_35_nonpromoted_cli'] = df['Percentile_35_nonpromoted_cli'] 
main['Percentile_40_nonpromoted_cli'] = df['Percentile_40_nonpromoted_cli'] 
main['Percentile_45_nonpromoted_cli'] = df['Percentile_45_nonpromoted_cli'] 
main['Percentile_50_nonpromoted_cli'] = df['Percentile_50_nonpromoted_cli'] 
main['Percentile_55_nonpromoted_cli'] = df['Percentile_55_nonpromoted_cli']
main['Percentile_60_nonpromoted_cli'] = df['Percentile_60_nonpromoted_cli']
main['Percentile_65_nonpromoted_cli'] = df['Percentile_65_nonpromoted_cli'] 
main['Percentile_70_nonpromoted_cli'] = df['Percentile_70_nonpromoted_cli'] 
main['Percentile_75_nonpromoted_cli'] = df['Percentile_75_nonpromoted_cli']
main['Percentile_80_nonpromoted_cli'] = df['Percentile_80_nonpromoted_cli'] 
main['Percentile_85_nonpromoted_cli'] = df['Percentile_85_nonpromoted_cli'] 
main['Percentile_90_nonpromoted_cli'] = df['Percentile_90_nonpromoted_cli']
main['Percentile_95_nonpromoted_cli'] = df['Percentile_95_nonpromoted_cli'] 
main['Percentile_98_nonpromoted_cli'] = df['Percentile_98_nonpromoted_cli'] 
main['Most Common Price_1_nonpromoted_cli'] = currency + " " + df['Most Common Price_1_nonpromoted_cli'] # df['MODE_1_nonpromoted_cli'] 
main['Most Common Price_2_nonpromoted_cli'] = currency + " " + df['Most Common Price_2_nonpromoted_cli'] # df['MODE_2_nonpromoted_cli'] 
main['Most Common Price_3_nonpromoted_cli'] = currency + " " + df['Most Common Price_3_nonpromoted_cli'] # df['MODE_3_nonpromoted_cli']
main['Most Common Price_4_nonpromoted_cli'] = currency + " " + df['Most Common Price_4_nonpromoted_cli'] # df['MODE_4_nonpromoted_cli']
main['Most Common Price_5_nonpromoted_cli'] = currency + " " + df['Most Common Price_5_nonpromoted_cli'] # df['MODE_5_nonpromoted_cli']
main['Most Common Price_UNITS_1_nonpromoted_cli'] = df['Most Common Price_UNITS_1_nonpromoted_cli'] # df['MCP_PCT_UNITS_1_nonpromoted_cli']
main['Most Common Price_UNITS_2_nonpromoted_cli'] = df['Most Common Price_UNITS_2_nonpromoted_cli'] # df['MCP_PCT_UNITS_2_nonpromoted_cli']
main['Most Common Price_UNITS_3_nonpromoted_cli'] = df['Most Common Price_UNITS_3_nonpromoted_cli'] # df['MCP_PCT_UNITS_3_nonpromoted_cli']
main['Most Common Price_UNITS_4_nonpromoted_cli'] = df['Most Common Price_UNITS_4_nonpromoted_cli'] # df['MCP_PCT_UNITS_4_nonpromoted_cli']
main['Most Common Price_UNITS_5_nonpromoted_cli'] = df['Most Common Price_UNITS_5_nonpromoted_cli'] # df['MCP_PCT_UNITS_5_nonpromoted_cli']

main['Percentile_2_nonpromoted_rem'] = df['Percentile_2_nonpromoted_rem']
main['Percentile_5_nonpromoted_rem'] = df['Percentile_5_nonpromoted_rem']
main['Percentile_10_nonpromoted_rem'] = df['Percentile_10_nonpromoted_rem']
main['Percentile_15_nonpromoted_rem'] = df['Percentile_15_nonpromoted_rem']
main['Percentile_20_nonpromoted_rem'] = df['Percentile_20_nonpromoted_rem']
main['Percentile_25_nonpromoted_rem'] = df['Percentile_25_nonpromoted_rem']
main['Percentile_30_nonpromoted_rem'] = df['Percentile_30_nonpromoted_rem']
main['Percentile_35_nonpromoted_rem'] = df['Percentile_35_nonpromoted_rem']
main['Percentile_40_nonpromoted_rem'] = df['Percentile_40_nonpromoted_rem']
main['Percentile_45_nonpromoted_rem'] = df['Percentile_45_nonpromoted_rem']
main['Percentile_50_nonpromoted_rem'] = df['Percentile_50_nonpromoted_rem']
main['Percentile_55_nonpromoted_rem'] = df['Percentile_55_nonpromoted_rem']
main['Percentile_60_nonpromoted_rem'] = df['Percentile_60_nonpromoted_rem']
main['Percentile_65_nonpromoted_rem'] = df['Percentile_65_nonpromoted_rem']
main['Percentile_70_nonpromoted_rem'] = df['Percentile_70_nonpromoted_rem']
main['Percentile_75_nonpromoted_rem'] = df['Percentile_75_nonpromoted_rem']
main['Percentile_80_nonpromoted_rem'] = df['Percentile_80_nonpromoted_rem']
main['Percentile_85_nonpromoted_rem'] = df['Percentile_85_nonpromoted_rem']
main['Percentile_90_nonpromoted_rem'] = df['Percentile_90_nonpromoted_rem']
main['Percentile_95_nonpromoted_rem'] = df['Percentile_95_nonpromoted_rem']
main['Percentile_98_nonpromoted_rem'] = df['Percentile_98_nonpromoted_rem']
main['Most Common Price_1_nonpromoted_rem'] = currency + " " + df['Most Common Price_1_nonpromoted_rem'] # df['MODE_1_nonpromoted_rem'] 
main['Most Common Price_2_nonpromoted_rem'] = currency + " " + df['Most Common Price_2_nonpromoted_rem'] # df['MODE_2_nonpromoted_rem'] 
main['Most Common Price_3_nonpromoted_rem'] = currency + " " + df['Most Common Price_3_nonpromoted_rem'] # df['MODE_3_nonpromoted_rem']
main['Most Common Price_4_nonpromoted_rem'] = currency + " " + df['Most Common Price_4_nonpromoted_rem'] # df['MODE_4_nonpromoted_rem']
main['Most Common Price_5_nonpromoted_rem'] = currency + " " + df['Most Common Price_5_nonpromoted_rem'] # df['MODE_5_nonpromoted_rem']
main['Most Common Price_UNITS_1_nonpromoted_rem'] = df['Most Common Price_UNITS_1_nonpromoted_rem'] # df['MCP_PCT_UNITS_1_nonpromoted_rem']
main['Most Common Price_UNITS_2_nonpromoted_rem'] = df['Most Common Price_UNITS_2_nonpromoted_rem'] # df['MCP_PCT_UNITS_2_nonpromoted_rem']
main['Most Common Price_UNITS_3_nonpromoted_rem'] = df['Most Common Price_UNITS_3_nonpromoted_rem'] # df['MCP_PCT_UNITS_3_nonpromoted_rem']
main['Most Common Price_UNITS_4_nonpromoted_rem'] = df['Most Common Price_UNITS_4_nonpromoted_rem'] # df['MCP_PCT_UNITS_4_nonpromoted_rem']
main['Most Common Price_UNITS_5_nonpromoted_rem'] = df['Most Common Price_UNITS_5_nonpromoted_rem'] # df['MCP_PCT_UNITS_5_nonpromoted_cli']

main['Percentile_2_promoted_cli'] = df['Percentile_2_promoted_cli']
main['Percentile_5_promoted_cli'] = df['Percentile_5_promoted_cli']
main['Percentile_10_promoted_cli'] = df['Percentile_10_promoted_cli']
main['Percentile_15_promoted_cli'] = df['Percentile_15_promoted_cli']
main['Percentile_20_promoted_cli'] = df['Percentile_20_promoted_cli']
main['Percentile_25_promoted_cli'] = df['Percentile_25_promoted_cli']
main['Percentile_30_promoted_cli'] = df['Percentile_30_promoted_cli'] 
main['Percentile_35_promoted_cli'] = df['Percentile_35_promoted_cli'] 
main['Percentile_40_promoted_cli'] = df['Percentile_40_promoted_cli'] 
main['Percentile_45_promoted_cli'] = df['Percentile_45_promoted_cli'] 
main['Percentile_50_promoted_cli'] = df['Percentile_50_promoted_cli'] 
main['Percentile_55_promoted_cli'] = df['Percentile_55_promoted_cli']
main['Percentile_60_promoted_cli'] = df['Percentile_60_promoted_cli']
main['Percentile_65_promoted_cli'] = df['Percentile_65_promoted_cli'] 
main['Percentile_70_promoted_cli'] = df['Percentile_70_promoted_cli'] 
main['Percentile_75_promoted_cli'] = df['Percentile_75_promoted_cli']
main['Percentile_80_promoted_cli'] = df['Percentile_80_promoted_cli'] 
main['Percentile_85_promoted_cli'] = df['Percentile_85_promoted_cli'] 
main['Percentile_90_promoted_cli'] = df['Percentile_90_promoted_cli']
main['Percentile_95_promoted_cli'] = df['Percentile_95_promoted_cli'] 
main['Percentile_98_promoted_cli'] = df['Percentile_98_promoted_cli'] 
main['Most Common Price_1_promoted_cli'] = currency + " " + df['Most Common Price_1_promoted_cli'] # df['MODE_1_promoted_cli']
main['Most Common Price_2_promoted_cli'] = currency + " " + df['Most Common Price_2_promoted_cli'] # df['MODE_2_promoted_cli']
main['Most Common Price_3_promoted_cli'] = currency + " " + df['Most Common Price_3_promoted_cli'] # df['MODE_3_promoted_cli']
main['Most Common Price_4_promoted_cli'] = currency + " " + df['Most Common Price_4_promoted_cli'] # df['MODE_4_promoted_cli']
main['Most Common Price_5_promoted_cli'] = currency + " " + df['Most Common Price_5_promoted_cli'] # df['MODE_5_promoted_cli']
main['Most Common Price_UNITS_1_promoted_cli'] = df['Most Common Price_UNITS_1_promoted_cli'] # df['MCP_PCT_UNITS_1_promoted_cli']
main['Most Common Price_UNITS_2_promoted_cli'] = df['Most Common Price_UNITS_2_promoted_cli'] # df['MCP_PCT_UNITS_2_promoted_cli'] 
main['Most Common Price_UNITS_3_promoted_cli'] = df['Most Common Price_UNITS_3_promoted_cli'] # df['MCP_PCT_UNITS_3_promoted_cli'] 
main['Most Common Price_UNITS_4_promoted_cli'] = df['Most Common Price_UNITS_4_promoted_cli'] # df['MCP_PCT_UNITS_4_promoted_cli'] 
main['Most Common Price_UNITS_5_promoted_cli'] = df['Most Common Price_UNITS_5_promoted_cli'] # df['MCP_PCT_UNITS_5_promoted_cli']

main['Percentile_2_promoted_rem'] = df['Percentile_2_promoted_rem']
main['Percentile_5_promoted_rem'] = df['Percentile_5_promoted_rem'] 
main['Percentile_10_promoted_rem'] = df['Percentile_10_promoted_rem'] 
main['Percentile_15_promoted_rem'] = df['Percentile_15_promoted_rem'] 
main['Percentile_20_promoted_rem'] = df['Percentile_20_promoted_rem'] 
main['Percentile_25_promoted_rem'] = df['Percentile_25_promoted_rem'] 
main['Percentile_30_promoted_rem'] = df['Percentile_30_promoted_rem'] 
main['Percentile_35_promoted_rem'] = df['Percentile_35_promoted_rem'] 
main['Percentile_40_promoted_rem'] = df['Percentile_40_promoted_rem'] 
main['Percentile_45_promoted_rem'] = df['Percentile_45_promoted_rem'] 
main['Percentile_50_promoted_rem'] = df['Percentile_50_promoted_rem'] 
main['Percentile_55_promoted_rem'] = df['Percentile_55_promoted_rem'] 
main['Percentile_60_promoted_rem'] = df['Percentile_60_promoted_rem'] 
main['Percentile_65_promoted_rem'] = df['Percentile_65_promoted_rem'] 
main['Percentile_70_promoted_rem'] = df['Percentile_70_promoted_rem'] 
main['Percentile_75_promoted_rem'] = df['Percentile_75_promoted_rem'] 
main['Percentile_80_promoted_rem'] = df['Percentile_80_promoted_rem'] 
main['Percentile_85_promoted_rem'] = df['Percentile_85_promoted_rem'] 
main['Percentile_90_promoted_rem'] = df['Percentile_90_promoted_rem'] 
main['Percentile_95_promoted_rem'] = df['Percentile_95_promoted_rem'] 
main['Percentile_98_promoted_rem'] = df['Percentile_98_promoted_rem'] 
main['Most Common Price_1_promoted_rem'] = currency + " " + df['Most Common Price_1_promoted_rem'] # df['MODE_1_promoted_rem']
main['Most Common Price_2_promoted_rem'] = currency + " " + df['Most Common Price_2_promoted_rem'] # df['MODE_2_promoted_rem']
main['Most Common Price_3_promoted_rem'] = currency + " " + df['Most Common Price_3_promoted_rem'] # df['MODE_3_promoted_rem']
main['Most Common Price_4_promoted_rem'] = currency + " " + df['Most Common Price_4_promoted_rem'] # df['MODE_4_promoted_rem']
main['Most Common Price_5_promoted_rem'] = currency + " " + df['Most Common Price_5_promoted_rem'] # df['MODE_5_promoted_rem']
main['Most Common Price_UNITS_1_promoted_rem'] = df['Most Common Price_UNITS_1_promoted_rem'] # df['MCP_PCT_UNITS_1_promoted_rem']
main['Most Common Price_UNITS_2_promoted_rem'] = df['Most Common Price_UNITS_2_promoted_rem'] # df['MCP_PCT_UNITS_2_promoted_rem']
main['Most Common Price_UNITS_3_promoted_rem'] = df['Most Common Price_UNITS_3_promoted_rem'] # df['MCP_PCT_UNITS_3_promoted_rem']
main['Most Common Price_UNITS_4_promoted_rem'] = df['Most Common Price_UNITS_4_promoted_rem'] # df['MCP_PCT_UNITS_4_promoted_rem']
main['Most Common Price_UNITS_5_promoted_rem'] = df['Most Common Price_UNITS_5_promoted_rem'] # df['MCP_PCT_UNITS_5_promoted_rem']

# Create 'Category Level Share'
main['share_cat_nonpromoted_cli'] = df['share_cat_nonpromoted_cli']
main['share_cat_nonpromoted_rem'] = df['share_cat_nonpromoted_rem']
main['share_cat_promoted_cli'] = df['share_cat_promoted_cli']
main['share_cat_promoted_rem'] = df['share_cat_promoted_rem']

# Create 'Storeweek Counts'
main['count_storeweeks_nonpromoted_cli'] = df['count_storeweeks_nonpromoted_cli']
main['count_storeweeks_nonpromoted_rem'] = df['count_storeweeks_nonpromoted_rem']
main['count_storeweeks_promoted_cli'] = df['count_storeweeks_promoted_cli']
main['count_storeweeks_promoted_rem'] = df['count_storeweeks_promoted_rem']

# Create 'Control'
main['transparency_issue'] 		= df['transparency_issue']
main['num_retailers_benchmark'] = df['num_retailers_benchmark']
main['Common product'] 			= df['Common product']




print('\t[Done] Data formatted successfully')
print('')