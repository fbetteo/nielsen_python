# Pandas - Clase 3

## Función DataFrame

La función `DataFrame` de pandas, permite crear un Data Frame desde el código.

### Diccionario de Columnas

Puedo constuir un Data Frame pasando un diccionario con listas (o numpy arrays) que sean las columnas.

```python
import pandas as pd
pd.DataFrame(
    {
        "a":[4,5,6],
        "b":[7,8,9],
        "c":[10,11,12]
    },
    index=['uno','dos','tres']
)
```

En este caso vamos a estar crean un Data Frame con las columnas _a_, _b_ y _c_. Opcionalmente vamos a agregar el índice con el arugmento `index`.

### Matriz

Si le pasamos como argumento una lista de listas de igual longitud, se interpreta que es una matriz.

```python
pd.DataFrame(
    [[4,7,10],
     [5,8,11],
     [6,9,12]],
     columns=['a','b','c'],
     index=['uno','dos','tres']
)
```

Vemos que los Data Frames son iguales pero en el primer caso las pasamos por columna y en el segundo por fila.

### Lista de Diccionarios

```python
pd.DataFrame(
    [
        {'a':4,'b':7,'c':10},
        {'a':5,'b':8,'c':11},
        {'a':6,'b':9,'c':12}
    ]
)
```

## pivot_table

La función `pivot_table` toma un DataFrame y genera una Tabla Pivot.

Los argumentos son:

* _columns:_ Variables que van a agrupar en las columnas.
* _index:_ Variables que van a agrupar en las filas.
* _values:_ Variables que van a ser sumarizadas.
* _aggfunc:_ Funciones de sumarización.
* _fill\_value:_ Valor a reemplazar en caso de no existir la combinación columns-index.

```python
import os
df = pd.read_csv(os.path.join('data','Vb_semanalizadas.csv'))

df.pivot_table(columns=["variable"],
               index=["month",'year'],
               aggfunc=["sum","mean"],
               values=['value','ValueSplit'])
```

## pivot

La función `pivot` toma un DataFrame y pivotea (transpone) por una variable.

No debe haber duplicados de las variables que van en filas y columnas.

Los argumentos son los mismos que los de `pivot_table` sin `aggfunc`.

```python
df.loc[df.month=='December'].pivot(index='Date',
                                   columns='variable',
                                   values='value')
```

## melt

La función `melt` pasa una tabla de formato `wide` a `long`, es decir, hace lo opuesto que la función `pivot`.

Los argumentos que toma son:

* _id\_vars:_ Variables que van a mantenerse igual.
* _value\_vars:_ Variables que van a pasar de ser varias a ser una sola.
* _value\_name:_ Nombre que va a tomar la nueva columna que es la unificación de _value\_vars_.
* _var_name:_ Nombre que va a tomar la nueva columna que indica la variable original.
* _fill\_value:_ Valor a reemplazar en caso de no existir la combinación columns-index.

```python
df.melt(id_vars=["variable","Date"],
        value_vars=["value","ValueSplit"],
        value_name="valor",
        var_name="metrica")
```

## merge

Para unir horizontalmente 2 DataFrames se utiliza la función `merge`.

Los parámetros que toma son:

* DataFrame a mergear.
* _on:_ Variables por las cuales mergear.
* _left\_on::_  Variables del DataFrame a la izquierda.
* _right\_on::_  Variables del DataFrame a la derecha.
* _how:_ Tipo de Merge:
    * _inner:_ Sólo va a conservar los registros que tengan observación de la variable por la cual hacemos el merge en ambos DataFrames.
    * _left:_ Sólo va a conservar los registros del DataFrame de la derecha que tengan observación de la variable por la cual hacemos el merge y todos los del de la izquierda.
    * _right:_ Sólo va a conservar los registros del DataFrame de la izquierda que tengan observación de la variable por la cual hacemos el merge y todos los del de la derecha.
    * _outer:_ Conserva todos los registros.

```python
universe = pd.read_csv(os.path.join('data','UNIVERSE.csv'))
print(universe.head())

regiones = pd.read_csv(os.path.join('data','REGION_lkp.csv'))
print(regiones)

universe.merge(regiones,on='REGION',how='left')
```

## concat

La función `concat` toma una lista de DataFrames y los une por un eje.

```python
pr1 = pd.read_csv(os.path.join('data','PromoReportVariables.csv'))
pr2 = pd.read_csv(os.path.join('data','PromoReportClaves1.csv'))
pr3 = pd.read_csv(os.path.join('data','PromoReportClaves2.csv'))
```

Para unir verticalmente (una encima de la otra) debemos colocar el parámetro `axis=0`.

```python
pr4 = pd.concat([pr2,pr3],axis=0,ignore_index=True)
```

>Dado que comparten índices, nos conviene poner el parámetro `ignore_index=True`.

Para unir horizontalmente (una al lado de la otra) debemos colocar el parámetro `axis=1`.

```python
pr5 = pd.concat([pr4,pr1],axis=1)
```

## shift

La función `shift` nos devuelve el valor de una variable con el lag que le indiquemos.

Es importante que el DataFrame esté ordenado

```python
df.assign(**{'2 antes':df.value.shift(2),
             '3 después':df.value.shift(-3)
             })
```

>La sitaxis `**diccionario` permite pasar un diccionario con los parámetros y los valores de lo mismos a una función.

## expanding

La función `expanding` nos permite calcular funciones de ventana desde el inicio de un grupo.

```python
df.groupby('variable').value.expanding().max()
```

## rolling

La función `rolling` nos permite calcular funciones de ventana móvil.

```python
df.groupby('variable').value.rolling(3).mean()
```

## to_csv

Para exportar a un archivo de texto, la función es `to_csv`.

```python
df.to_csv('Nombre del Archivo.csv',index=False)
```

## Ejercicio

Continuar el ejercicio de la clase pasada pero en el archivo `formateador_PI Mirek_Parte_II.py`.

Agregarle que guarde resultados en algún lado.