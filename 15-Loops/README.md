# Loops

## Iterators

Los `iterators` son objetos sobre los que se puede iterar.

## for

Con el `for` loop que vamos a poder indicarle al sistema sobre qué objeto tiene que iterar.

En cada iteración se generará una variable y se ejecutará el código. Tener en cuenta que las variables generadas persistirán a la salida del loop y si no hay una asignacion o una impresión, se perderá todo lo que suceda en iteraciones intermedias.

### Listas

Le vamos a indicar cuál va a ser la variable a la cual vamos a asignar cada elemento de la lista con la sintaxis `for <alias> in <lista>:` y en el bloque de código se usará el alias como una variable más.

```python
numeros = [1,2,3,4,5]
for n in numeros:
    print(n)
```

### zip

```python
a=[1,2,3]
b=[4,5,6]
for i in zip(a,b):
    print(i)
```

La función `zip` toma 2 iterators y genera un nuevo iterator compuesto por tuplas de los correspondientes elementos de los iterators.

Valiéndonos de `variable unpacking` podemos pasar 2 índices y trabajar con ellos.

```python
for i,j in zip(a,b):
    print(i**j)
```

### enumerate

```python
for i,n in enumerate(numeros):
    print('{}:{}'.format(i,n))
```

La función `enumerate` genera un iterator que cada elemento es una tupla `(orden,elemento)`.

### range

La función `range` va a generar una iterator conteniendo una secuencia numérica con la siguiente sintaxis `range([inicio,] fin [,step])` cumpliendo las mismas reglas del slicing.

```python
for i in range(30,1,-3):
    print(i)
```

### .keys

La función devuelve las claves de un diccionario y se puede iterar como si fuese una lista.

```python
dic = {'uno':'one','dos':'two','tres':'three'}
for k in dic.keys():
    print(k)
```

### ,values

La función devuelve los valores de un diccionario y se puede iterar como si fuese una lista.

```python
for v in dic.values():
    print(v)
```

### .items

La función devuelve los tuplas clave-valor de un diccionario y se puede iterar como si fuese una lista.

```python
for k,v in dic.items():
    print('{}:{}'.format(v,k))
```

## While

El `while` loop itera mientras se cumpla la condición. Como en todos los casos, hay que tener cuidado con el loop infinito, es decir, que siempre se cumpla la condición del while.

La sintaxis es `while <condición>:` y debajo con tabulación, el código a ejecutar.

```python
fibonacci = [1,1]
while len(fibonacci)<10:
    fibonacci.append(sum(fibonacci[-2:]))
fibonacci
```

## break

La sentencia `break` sirve para terminar el loop, 

Es una forma de garantizar la primera ejecución del código.

```python
fibonacci = [1,1]
while True:
    fibonacci.append(sum(fibonacci[-2:]))
    if len(fibonacci)>10:
        break
fibonacci
```

## List comprehension

`List comprehension` es lo más pythonic (sí, la manga de nardos le puso pythonic a hacer cosas de forma pythonera) que hay.

Al principio cuesta entenderla, pero al final no se lo para de usar, como esos videos de Life Hacks que aparecen en Youtube y después es imposible parar de mirar.

Lo que hacemos es crear una lista con una sintaxis particular, vamos de menor a mayor.

```python
[a for a in range(10)]
```

En este caso estamos creando una lista que replica los elementos de un iterator.

```python
[a**2 for a in range(10)]
```

Acá estamos modificando los elementos de un iterator

```python
[a**2 if a%2==0 else -a for a in range(10)]
```

Acá estamos generando una función más compleja

```python
[a for a in range(10) if a%2==1]
```

En este caso estamos filtrando casos del iterator.

La sintaxis es `[<expresión del ítem> for <ítem> in <iterator> if <condición>]`

Veamos más ejemplos

```python
[k*v for k,v in zip([1,2,3,4],[10,20,30,40]) if k%2==1]
```

## Dict Comprehension

En el caso de Dict comprehension, se pondrá entre `{}` la misma sintaxis, garantizando que haya una asignación a un par clave-valor.

```python
{k:v for k,v in zip([1,2,3,4],[10,20,30,40]) if k%2==1}
```