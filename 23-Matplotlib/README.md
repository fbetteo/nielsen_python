# Matplotlib

`matplotlib` es una librería gráfica de Python que, de acuerdo a sus desarrolladores

>produces publication quality figures in a variety of hardcopy formats and interactive environments across platforms.

## Conceptos generales

Graficar requiere de varios niveles, desde el más general (hacer un contorno) a lo más específico (este pixel tiene que ser rojo).

Todo en `matplotlib` está organizado en jerarquías.

## Librería

Para trabajar con `matplotlib` vamos a importar `matplotlib.pyplot` y con el alias `plt`.

```python
import matplotlib.pyplot as plt
```

## Elementos

### Anatomía

![Elementos](img/anatomia.png)

### Figure

Una `figure` es la "imagen" generada con todos los plots que creemos.

![figure](img/figure_2x2.png)

### Axes

Los `axes` son la región de la imagen que contiene la representación de los datos.

Los `axes` contienen objetos `Axis` (ejes).

Poseen un título, y un label de cada eje.

### Axis

Los `Axis` se ocupan de los límites de los datos a representar y los `ticks` (marcas) y los `ticklabels` (texto que marca cuál es el valor que representa el tick).

## Cómo funciona?

Para cada función en el módulo `pyplot` hay una _figure_ y un _axes_ sobre el cual accionar.

Cuando no hay uno sobre el cual está trabajando, lo crea.

```python
import numpy as np

x = np.linspace(0, 2, 100)

#Esta función va a crear una Figure, un Axes y va a agregarle una representación de datos
plt.plot(x, x, label='linear')

#Estas funciones van a agregar representaciones de datos sobre el Figure y el Axes que está trabajando
plt.plot(x, x**2, label='quadratic')
plt.plot(x, x**3, label='cubic')

#Agrega labels a los Axis
plt.xlabel('x label')
plt.ylabel('y label')

#Agrega un título
plt.title("Simple Plot")

#Agrega la leyenda
plt.legend()

#Muestra el gráfico
plt.show()
```

Una vez que se ejecuta la función `show` el gráfico se "elimina".

## subplots

La función `subplots` genera una `figure` y sus respectivos `axes`.

Para agregar un elemento a un `axes`, tenemos que usar los métodos del mismo.

```python
x = np.arange(0, 10, 0.2)
y = np.sin(x)
fig, ax = plt.subplots()
ax.plot(x, y)
plt.show()
```

En caso de querer agregar múltiples axes se agregan los argumentos `nrows` y `ncols`.

```python
fig,axes = plt.subplots(nrows=2,ncols=2)

axes[0,0].plot(np.array([0,1]),np.array([1,0]))
axes[0,1].plot(np.array([0.5,0.5]),np.array([1,0]))
axes[1,0].plot(np.array([0,10]),np.array([1,0]))

axes[0,0].set_title('Línea Descendente')
axes[1,1].set_xlabel('Nada por aquí')
axes[1,1].set_ylabel('Nada por allá')
plt.show()
```

## plot

La función `plot` genera gráficos de línea por defecto.

Si le paso sólo 1 vector a graficar, va a tomar el índice como la variable a representar en el eje _x_ y va a representar el vector en el eje y.

```python
fig,axes = plt.subplots()
x=np.arange(0,10,2)
y=x**2
axes.plot(x,y)
plt.show()
```

Por defecto va a unir por línea los datos.

```python
fig,axes = plt.subplots()
x=np.arange(20)
np.random.seed(42)
np.random.shuffle(x)
y=x**2
axes.plot(x,y)
plt.show()
```

Para modificar cómo representa los datos, podemos utilizar un 3er argumento que combina marker y color.

```python
fig,axes = plt.subplots(2,2)
x=np.arange(20)
np.random.seed(42)
np.random.shuffle(x)
y=x**2
axes[0,0].plot(x,y,'ro')
axes[0,1].plot(x,y,'g-')
axes[1,0].plot(x,y,'bx')
axes[1,1].plot(x,y,'y--')
plt.show()
```

También se pueden usar las propiedades `color`, `marker`, `linestyle`, `linewidth` y `markersize` para tener más control.

```python
fig,axes = plt.subplots()
axes.plot(x,y,color='red', marker = 'o', linestyle='dashed',linewidth = 2, markersize=10)
plt.show()
```

Las variables también pueden agruparse en un diccionario y pasar el mismo bajo el parámetro `data`

```python
np.random.seed(42)
data= {"x":np.random.uniform(0,10,20),
"y":np.random.normal(0,1,20)}

fig,axes = plt.subplots()
axes.plot("x","y","r+",data=data)
plt.show()
```

## scatter

La función `scatter` nos permite crear un scatterplot cuyos `markers` están modificados por otras variables.

```python
x=np.random.uniform(0,10,50)
y=np.random.normal(0,1,50)
size = np.random.uniform(10,50,50)
color = np.random.uniform(0,20,50)

fig,axes = plt.subplots()
axes.scatter(x,y,s=size,c=color)
plt.show()
```

## bar

La función `bar` genera gráficos de barras.

```python
categorias = ['SILLAS','MESAS','BICICLETAS','BIBLIOTECAS']
valores = [6,1,2,2]
fig,axes = plt.subplots()
axes.bar(categorias,valores)
plt.show()
```

## hist

La función `hist` genera un histograma.

```python
np.random.seed(42)
fig,axes = plt.subplots()
axes.hist(np.random.normal(200,30,10000),bins=100)
plt.show()
```

## text

El método `text` agrega un texto en las coordenadas indicadas.

Los argumentos de la función son `coordenada x`, `coordenada y`, `texto`.

Se pueden utilizar expresiones en TeX.

```python
np.random.seed(42)
fig,axes = plt.subplots()
axes.hist(np.random.normal(200,30,10000),bins=100)
axes.text(100,250,r'$\mu=200,\ \sigma=50$')
axes.grid(True)
plt.show()
```

## annotate

El método `annotate` genera anotaciones dentro del gráfico.

Los parámetros son:

* Texto de la anotación.
* _xy:_ Una tupla con la coordenada del punto sobre el cual se quiere hacer una anotación.
* _xytext:_ Una tupla con la posición del texto de la anotación.
* _arrowprops:_ Propiedades de la flecha

```python
x = np.arange(-10,10,0.1)
y= x**2

fig,axes = plt.subplots()
axes.plot(x,y)
axes.annotate("Punto Mínimo",(0,0),(-3,10),arrowprops={'facecolor':'black'})
axes.grid(True)
plt.show()
```

## Pandas

Los métodos de matplotlib pueden ser usados en Pandas.

```python
import pandas as pd
import os
iris = pd.read_csv(os.path.join('data','iris.csv'))

fig, axes = plt.subplots(3,1)
iris.plot.scatter(x="Sepal.Length",y="Sepal.Width",ax=axes[0])
iris.hist("Petal.Length",ax=axes[1])
iris.groupby('Species').size().reset_index(name='Cantidad').plot.bar("Species","Cantidad", ax=axes[2])
plt.show()
```

## Ejercicio

Hacer exploraciones sobre el archivo `op_rms_data.csv`.