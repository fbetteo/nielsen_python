# Funciones

## Sin parámetros

### Definición

Para crear una función propia que no lleve parámetros la sintaxis es `def nombreDeMiFuncion():` y debajo escribir lo que va a hacer.

```python
def holis():
    print('Hola Mundo')
```

> Tener en cuenta que una función tiene que tener una acción asociada, en este caso va a imprimir.

### Ejecutando

Para ejecutarla, se escribe `nombreDeMiDuncion()`.

```python
holis()
```

### return

La sentencia return va a devolver un valor.

```python
from datetime import datetime

def cuantoTarda():
    start = datetime.now()
    for i in range(100000):
        pass
    return datetime.now()-start
```

Lo podemos asignar a una variable:

```python
esto = cuantoTarda()
```

O bien tener el mismo resultado que si escribiésemos el código

```python
cuantoTarda()
```

Se puede combinar `print` con `return`.

```python

def hoy():
    dias = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo']
    hoy_es = datetime.today().weekday()
    print('Hoy es {h}, ayer fue {a}, anteayer fue {aa}, mañana va a ser {m} y pasado va a ser {pm}'.format(h=dias[hoy_es],
                     a=dias[(hoy_es-1)%7],
                     aa=dias[(hoy_es-2)%7],
                     m=dias[(hoy_es+1)%7],
                     pm=dias[(hoy_es+2)%7]))
    return dias[hoy_es]

hoyEs = hoy()

print(hoyEs)
```

Cuando en la definición encuentra el return, no continúa ejecutando el resto.

```python

def hoy():
    dias = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo']
    hoy_es = datetime.today().weekday()
    return dias[hoy_es]
    print('Hoy es {h}, ayer fue {a}, anteayer fue {aa}, mañana va a ser {m} y pasado va a ser {pm}'.format(h=dias[hoy_es],
                     a=dias[(hoy_es-1)%7],
                     aa=dias[(hoy_es-2)%7],
                     m=dias[(hoy_es+1)%7],
                     pm=dias[(hoy_es+2)%7]))

hoyEs = hoy()

print(hoyEs)
```

## Con parámetros

### Definición

La sintaxis es `def nombreDeMiFuncion(parametros separados por coma)`

```python
def diaDeSemana(weekdaynum):
    weekdays = {0:'Lunes',
                1:'Martes',
                2:'Miércoles',
                3:'Jueves',
                4:'Viernes',
                5:'Sábado',
                6:'Domingo'}
    return weekdays[weekdaynum%7]

print(diaDeSemana(4))
```

### Llamado por posición o clave

Al invocar una función que posee parámetros, podemos cargar los argumentos por:

1. __Posición:__ Se escriben los argumentos en el mismo orden que al momento de declarar la función.
2. __Clave:__ Se escribe `clave=valor`.

```python
def IMC(peso,altura):
    return peso/altura**2

#Por posición
print(IMC(80,1.78))

#Por clave
print(IMC(altura=1.78,peso=80))
```

Se puede hacer de forma mixta, aunque una vez que comenzamos a escribir por clave, se debe escribir por clave todos los argumentos:

```python
def ejemploPedorro(a,b,c,d):
    return a*b^c-d

print(ejemploPedorro(1,2,3,4))

print(ejemploPedorro(1,c=3,d=4,b=2))
```

```python
#Esto va a dar un error
print(ejemploPedorro(1,c=3,b=2,4))
```

### Valores por defecto

Para establecer un valor por defecto de algún parámetro, tengo que colocar al final cuál es ese valor.

```python
def potencia(base,exponente=2):
    return base**exponente

print(potencia(2))
print(potencia(2,3))
```

## Scope

Las variables al ser declaradas dentro de una función, tienen un alcance local. No puede ser utilizada por fuera.

```python
def scopeLocal():
    nueva_variable=1

scopeLocal()
print(nueva_variable)
```

Tampoco puede modificar una variable existente.

```python
nueva_variable=3
def scopeLocal():
    nueva_variable=1

scopeLocal()
print(nueva_variable)
```

### Listas y Diccionarios

Cuando una variable es una lista o un diccionario y fue creado antes de la llamada de la función, si se modifica este elemento dentro de la función los cambios impactan.

```python
diccionario_a_romper = {'a':1}

def rompo_diccionario():
    diccionario_a_romper['a']=5
    diccionario_a_romper['b']=10

rompo_diccionario()
diccionario_a_romper
```

```python
lista_a_romper = [0]

def rompo_lista():
    lista_a_romper[0]=20
    lista_a_romper.append('WAAAA')
rompo_lista()
lista_a_romper
```

## Invocación dentre de otra funcióm

Se puede invocar a una función dentro de otra, siempre que se hubiese declarado previamente.

```python
def diaDeSemana(weekdaynum):
    weekdays = {0:'Lunes',
                1:'Martes',
                2:'Miércoles',
                3:'Jueves',
                4:'Viernes',
                5:'Sábado',
                6:'Domingo'}
    return weekdays[weekdaynum%7]

def decisiones():
    from datetime import datetime
    hoy_es = datetime.now().weekday()
    if diaDeSemana(hoy_es) in ['Viernes','Sabado']:
        print('Me la doy en la pera')
        return True
    else:
        print('Hoy me guardo')
        return False

me_la_doy = decisiones()
```

## Devolver varios valores

Una función puede devolver varios valores en una tupla y al invocarla, podemos valernos de `variable unpacking` para asignar los resultados a múltiples variables.

```python
def conQuienSalgo():
    import random
    compas = ['Nico Canil','Fran Moreno','El Buja','Murias']
    salgo_con = compas[random.randint(0,3)]
    if salgo_con in ['Fran Moreno','El Buja']:
        return (salgo_con,True)
    else:
        return(salgo_con,False)

salgo_con,me_agarro_una_tranca_de_novela = conQuienSalgo()
```

>La librería random sirve para generar númros seudoaleatorios.

## Recursión

La recursión consiste en invocar a una función en su misma definición.

Para esto se necesitan 2 cosas:

1. Un caso base
2. Una llamada recursiva

```python
def factorial(numero):
    #Condiciones para calcular el factorial
    if numero<0:
        print('Tiene que ser Mayor o Igual a 0')
        return None
    elif int(numero)!=numero:
        print('Tiene que se Entero')
        return None
    #Casos base
    elif numero in [0,1]:
        return 1
    else:
        return numero * factorial(numero-1)

factorial(7)
```