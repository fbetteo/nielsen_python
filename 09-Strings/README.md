# Strings

## Expresión

Los textos en Python se expresan entre comillas simples o dobles.

## Caracteres especiales

Cuando quiero agregar un caracter especial, tengo que usar el símbolo `\` que es el escape character.

### Comillas
Si queremos agregar un una comilla doble o simple, tenemos 2 opciones.

1. Usar la otra comilla para encerrar el texto
```python 
print("Encierro entre comillas dobles para poder poner ' sin hacer bardo")
```
2. Usar el caracter de escape `\` 
```python
print('El caracter de escape es mejor porque se puede ser consistente con el código para escribir \' o bien \" dependiendo el caso')
```

### Nueva Línea

Para agregar una nueva línea tengo que escribir `\n`

```python
print('Esto es una línea\ny esto es otra línea')
```

### Tabulación

La tabulación se simboliza como `\t`

```python
print('Texto\tseparado\tpor\t\ttabs')
```

## Strings multilínea

Puedo generar strings multilínea si los encierro entre `3 comillas`.

```python
poesia = """Qué es lo que hace un taxista seduciendo a la vida
            Qué es lo que hace un taxista construyendo una herida
            Qué es lo que hace un taxista enfrente de una dama
            Qué es lo que hace un taxista con sus sueños de cama"""
```
En este caso, todo lo que esté entre estas 3 comillas dobles será tomado literal (inclusive espacios agregados para hacer el código más prolijo por ejemplo).

## Largo

La función `len` nos permite saber cuál es el largo del texto

```python
len(poesia)
```

## Modificar Texto

### Pasar a mayúscula

Se utiliza el método `upper`

```python
'el que no levanta las manos maneja el patrullero'.upper()
```

>Los métodos en Python pueden tener un output o bien, modificar el objeto sobre el cual se corrió el método. En este caso, tiene un output.

### Pasa a minúscula

Se utiliza el método `lower`

```python
'ESCRIBIR TODO EN MAYÚCULA ES COMO GRITAR'.lower()
```

### Cambiar el case

Se utiliza el método `swapcase`

```python
'aLGUIEN SABE POR QUÉ TENGO UNA LUCECITA PRENDIDA EN EL TECLADO Y AHORA SE ESCRIBE TODO ASÍ?'.swapcase()
```

### Eliminar espacios al principio

Se utiliza el método `lstrip`

```python
'    Tampoco esperen un chiste en todos los ejemplos, esto no es 9gag'.lstrip()
```

### Eliminar espacios al final

Se utiliza el método `rstrip`

```python
'De hecho, los memes en las presentaciones me parece un recurso que se gastó muy rápido y quieren compensar falta de onda con un chiste hecho por otro    '.rstrip()
```

### Eliminar espacios al principio y al final

Se utiliza el método `strip`

```python
'    Ojo, tal vez sea un buen método para cortar la solemnidad del asunto, pero el abuso puede ser contraproducente, me imagino algún meme que eventualmente envejezca mal, debido a que partía de una \"incorrección política\" socialmente aceptada que comenzó a ser cuestionada, y es sabido que una vez hecha una presentación, difícilmente se cambie    '.strip()
```

### Reemplazar

Se utiliza método `replace` con los parámetros `substring original` y `nuevo subtring`.

```python
poesia.replace('taxista','Technical Expert')
```

## Analizar texto

### Chequear si substring pertenece al string

Se utiliza la sintaxis `<substring> in <string>` y nos va a devolver `True` o `False`.

```python
'Hello' in 'Hello World'
```

```python
'world' in 'Hello World'
```

### Ver en qué posición está la primer ocurrencia de un substring

Se utiliza el método 'find'

```python
poesia.find('taxista')
```

>Tener en cuenta que el primer índice en Python es el 0, así que el índice 22 corresponde al 23er caracter.

### Ver si comienza con un subtring

Se utiliza el método `startswith`

```python
'Toda buena mañana'.startswith('un buen desayuno')
```

```python
'Toda buena mañana'.startswith('Toda')
```

### Ver si finaliza con un substring

```python
'Toda buena mañana'.endswith('.')
```

```python
'Toda buena mañana'.endswith('ana')
```

## Concatenar texto

Para concatenar texto, tengo que usar el operador `+`

```python
'esto es un texto' + 'concatenado'
```

Si quiero concatenar un número, primero tengo que pasarlo a string con la función `str`

```python
'Las clases de Python son tan entretenidas que no puedo creer que duren ' + str(2) + ' horas, el tiempo se pasa volando cuando uno se divierte'
```

## Format

El método format nos permite dejar placeholders en el texto y de ese modo parametrizarlo.

La forma más simple es escribir `{}` en nuestro string y luego en el método el texto a reemplazar.

```python
'Usar el método {} es muy {}!!!'.format('format','ñoñazo')
```

Lo bueno de esto es que no hace falta pasar a `str` los números.

```python
'Si me hubiesen dado {} centavos cada vez que me olvidé de pasar a string, hoy tendría {} pesos'.format(10,1.4)
```

Y además se les puede dar formato a los números.

```python
'Una tasa de {}% nominal anual implica una tasa de {:.2%} efectiva anual, ojo con el TNA'.format(60,(1+60/100/12)**12-1)
```

## Slicing

Para extraer subcadenas de texto, podemos poner entre `[]` los índices deseados.

Obtener el primer caracter:

```python
'Esto es un string'[0]
```

Obtener el cuarto caracter:

```python
'Esto es un string'[3]
```

>Los índices empiezan en el 0

Obtener el del 3er al 6to caracter:

```python
'Esto es un string'[2:6]
```

>Los límites del slicing son inclusivo el inferior, exclusivo el superior, por eso en nuestro ejemplo incluye el índice 2, pero no el 6 (7mo caracter)

Obtener el último caracter

```python
'Esto es un string'[-1]
```

Obtener el penúltimo caracter

```python
'Esto es un string'[-2]
```

Para entender los índices reversos, acá una ayuda:

|Posición                     |1ero|2do|3ero|4to|5to|6to|7mo|8vo|9no|10mo|11mo|12mo|
|                  ---   |-----|-----|-----|----|----|----|----|----|----|----|----|----|
|Índice                     | 0   | 1   | 2   | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 |
|Texto                     | M   | o   | n   | t  | y  |    | P  | y  | t  | h  | o  | n  |
|Índice Reverso                     | -12 | -11 | -10 | -9 | -8 | -7 | -6 | -5 | -4 | -3 | -2 | -1 |

Se pueden mezclar los índices con los índices reversos.

Obtener hasta el 5to caracter:

```python
'Esto es un string'[:5]
```

Obtener desde el 5to caracter:

```python
'Esto es un string'[4:]
```

## Ejercicio

Tomando algún gráfico o salida de algún modelo y, usando lo aprendido con respecto a variables, operaciones matemáticas y strings, generar un texto el cual sea parametrizable y que muestre alguna conclusión (no vimos condicionales todavía, así que no se preocupen) enviable por mail.

Asegúrense de tener un texto de saludo y firmar.

Ejemplo

>Hola (inserte consultor):, cómo estás?
>
>Te adjunto la salida de (inserte modelo).
>
>Como conclusiones generales podemos destacar que ...
>
>Caricias Significativas
>
>(Tu Nombre)
><br>(Tu Puesto)

Cuanto más puedan reeemplazar por variables, mejor.

Le agregar ingredientes (como por ejemplo, la salida de un modelo viene con cierto ID que hay que reemplazar, o en minúsculas y lo pasan a mayúsculas o lo que quieran, dejen volar su imaginación).