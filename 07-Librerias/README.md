# Librerías

## Importación

### Todo un módulo

```python
import math
math.sqrt(100)
```

### Todo un módulo con un alias

```python
import math as m
m.sqrt(100)
```

### Importar sólo un objeto de la librería

```python
from math import log
log(100)
```

### Importar un objeto de la librería con un alias

```python
from math import log10 as logdiez
logdiez(100)
```

### Importar más de un objeto de la librería

```python
from math import e,cos
e
```

### Importar todos los objetos de una librería

```python
from math import *
sin(10)
```
