# Listas

## Definición

Una lista es un conjunto ordenado de elementos que se identifican por medio de un índice.

Su contenido puede ser cualquier objeto, inclusive otras listas.

## Creación

Para generar una lista, se deben poner entre `[]` los elementos que la componen separados por `,`

```python
movies = ['Monty Python and the Holy Grail','Life of Brian','The Meaning of Life']
```

También puedo generar una lista vacía, lo cual es útil en el caso de querer iterar y agregar elementos a medida que itero. Para ello, simplemente asignamos a la variable, los corchetes vacíos.

```python
vacia=[]
```

No todos los elementos de la lista tienen que ser del mismo tipo.

```python
import datetime
cualquiera = ['Asadito',datetime.datetime.now(),30,['Marucha','Chinchulines','Escabio']]
```
## Análisis

### Longitud

La función `len` nos va a devolver el largo de la lista.

```python
len(cualquiera)
```

### Ver si un elemento forma parte de la lista

La sintaxis `<elemento> in <lista>` nos va a devolver True si el elemento se encuentra en la lista o False en caso contrario.

```python
'Asadito' in cualquiera
```

### Buscar un índice

La función `index` del objeto nos devuelve en qué índice se encuentra un elemento. De no encontralo, nos devuelve un error.

```python
cualquiera.index(30)
```

### Máximo

Con la función `max` calculo el máximo de una lista.

Es importante que los componentes sean todos del mismo tipo.

```python
max([1,2,3,56,2])
```

### Mínimo

Con la función `min` calculo el mínimo de una lista.

Es importante que los componentes sean todos del mismo tipo.

```python
min([1,2,3,56,2])
```

### Suma

Con la función `sum` calculo la suma de una lista.

Es importante que los componentes sean todos numérics.

```python
sum([1,2,3,56,2])
```


Con la función `min` calculo el mínimo de una lista.
## Slicing

Las reglas de slicing son las mismas que las de los strings.

Para seleccionar un solo elemento, escribir el índice entre `[]`

```python
numeros = [1,2,3,4,5,6,7,8,9]
numeros[3]
```

Para seleccionar un rango, poner el inicio y el final (no inclusive).

```python
numeros[2:5]
```

Para seleccionar un elemento desde el final, los índices reversos comienzan desde el -1 para el último elemento y decreciendo hasta el primero.

```python
numeros[-3]
```

Si quiero todos los elementos hasta el n-ésimo, entre corchetes escribo [:n]

```python
numeros[:4]
```

Si quiero todos los elementos desde el índice n, entre corchetes escribo [n:]

```python
numeros[-3:]
```

### 3er elemento de Slicing

En los casos anteriores estábamos seleccionando elementos de menor a mayor índice con un incremento de 1.

Podemos seleccionar elementos en un orden diferente y con otro incremento de índice escribiendo un 2do `:` y luego de este indicar el paso.

Hay que tener en cuenta que al cambiar el orden de selección, cambian mis límites

```python
numeros[5:2:-1]
```

>Cuál es el primer índice que devolvió? Cuál es el último?

Puedo traer los elementos de 2 en 2 por ejemplo:

```python
numeros[1:7:2]
```

Si quiero traer todos los elementos hasta el n-ésimo en orden de 2 en 2:

```python
numeros[:6:2]
```

Si quiero traer todos los elementos desde el índice n en orden de 3 en 3:

```python
numeros[1::3]
```

Si quiero traer todos los elementos en orden inverso:

```python
numeros[::-1]
```

Generalizando el slicing se compone de 3 elementos:

```python
[
Límite Inferior, por defecto es 0                 :
Límite Superior, por defecto es len(lista) :
Step, por defecto es 1
]
```

## Modificando una lista

### Agregando elementos

#### append

El método `append` agregará el argumento al final de la lista.

```python
frases_de_murias = [
    'cómo venimos?',
    'a vos te parece?',
    'interpretá el espacio entre los \"ja\"'
]
frases_de_murias.append('señores...')
frases_de_murias
```

>Tener en cuenta que lo que se añadirá será del mismo tipo que el argumento, por lo cual si "appendeo" una lista, voy a tener una lista dentro de otra lista.

```python
frases_de_murias.append(['hay que comparar peras con peras no peras con manzanas','andá a dar una vuelta manzana y reflexioná sobre lo que acabás de decir'])
frases_de_murias
```

>Vean cómo después de la sintaxis estamos invocando el objeto. Esto es porque el método `append` modifica el objeto sobre el cual es ejecutado.

#### insert

El método `insert` nos permite insertar un elemento en el índice que le indiquemos, moviendo todos los elementos al índice siguiente.

```python
frases_de_murias.insert(1,'date una vuelta de manzana para bajar el enojo antes de contestar ese mail')
```

#### extend

```python
frases_de_murias.extend(['es muy temprano para eso, andáte y volvé a entrar','Vamos a doblecheckearlo'])
frases_de_murias
```

El método extend concatena listas. En vez de agregar una lista como elemento, va a agregar los elementos de la lista al final en nuevos índices sucesivos.

>Cuando "extendemos" un string, Python lo va a convertir en lista, y lo que va a agregar en índices sucesivos son sus caracteres.

```python
frases_de_murias.extend('uy')
frases_de_murias
```

### Modificando por slicing

Las reglas del slicing sirven para asignar elementos:

```python
frases_de_murias[-2:]=['acá no tenemos a su mamá que lava las tazas',
                       'andá a dar una vuelta manzana y reflexioná sobre lo que acabás de decir']
```

Si queremos asignar en un índice que no está en nuestra lista, vamos a tener un error.

```python
frases_de_murias[len(frases_de_murias)]='Basta!'
```

### Eliminando elementos

#### pop

El método `pop` va a eliminar un elemento por índice y lo va a devolver

```python
frases_de_murias.pop(5)
```

#### remove

El método `remove` sirve para eliminar un elemento por contenido.

```
frases_de_murias.remove('señores...')
```

>Se elimina la primer ocurrencia. De no encontrar coincidencia, devuelve un error.

#### del

La **sentencia** `del` elimina por índice.

Por qué una sentencia y no una función o método? Porque pintó.

```python
del frases_de_murias[2]
```

### Ordenando

El método `sort` va a ordenar la lista.

```python
frases_de_murias.sort()
```

Para ordenar de forma descendente, se debe usar el argumento `reverse=True`.

```python
frases_de_murias.sort(reverse=True)
```

## Operadores

### Concatenación

El operador `+` aplicado a listas va a concatenar y devolver las listas concatenadas.

```python
[1,2,3]+[5,6,7]
```

### Replicación

El operador `*` va a concatenar la lista consigo misma _n_ veces.

```python
[1,2,3]*3
```

## Listas y strings

### split

Para separar un string por un caracter, se usa la función `split` y nos va a devolver una lista con los substrings.

```python
'y dale Falcor dale dale Falcor'.split(' ')
```

### join

Para unir los elementos de una lista por un caracter, se utiliza la función `join`.

```python
','.join(['Don Ramón','Quico','El Chavo'])
```

## Alias

```python
a=[1,2,3]
b=a
```

Cuando realizamos una asignación como la reciente, estamos generando un alias, por lo tanto una modificación en un elemento de `b` va a modificar `a` y viceversa.

```
b.append(4)
a
```

Para realizar una copia del objeto se puede utilizar la función `copy` del objeto o bien, escribir `[:]` después del nombre del mismo.

```python
c=a.copy()
d=a[:]
c[0]='Holis'
d[-1]='Chauchis'
```

## Variables en lista

Si agregamos una variable a una lista, al modificar la variable, modificamos el contenido del elemento agregado a la lista.

```python
a=[1,2,3]
b=[4,5,6]
c=[a,b]
a.append(3)
c
```

