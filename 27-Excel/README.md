# Excel

## Pandas

Para trabajar con Pandas y Excel necesitamos instalar la librería `xlrd`, así que en nuestro environment ejecutamos 

```bash
pip install xlrd
```

### read_excel

Con la función de Pandas `read_excel` se importa una hoja de Excel a un DataFrame.

El funcionamiento es el mismo que en `read_csv`, pero ahora cobra relevancia el argumento `sheet_name` el cual puede ser:

* Nombre de la sheet
* Índice de la sheet
* Una lista mixta (en este caso va a generar un diccionario con DataFrames)

```python
import pandas as pd
import os

df = pd.read_excel(os.path.join('data','treemap argentina.xlsx'),sheet_name=0,skiprows=[0])

df.head()
```

### to_excel

En caso de querer crear un archivo con una sola hoja, basta con poner el nombre del archivo que vamos a exportar y el nombre de la hoja.

```python
df.to_excel(os.path.join('user','treemap_completo.xlsx'),sheet_name='PEQL')
```

Si queremos generar un archivo Excel con múltiples hojas, tenemos que crear un objeto de tipo `ExcelWriter` con la ruta del objeto. Tenemos que tener instalado `openpyxl`.

Es importante que hasta que no utilicemos la función `save` del ExcelWriter, no se va a generar ningún archivo.

```python
xl_writer = pd.ExcelWriter(os.path.join('user','treemap_separado.xlsx'), engine='openpyxl')

df[df.Provincia=='Global'].to_excel(xl_writer,sheet_name='Global',index=False)
df[df.Provincia!='Global'].to_excel(xl_writer,sheet_name='No Global',index=False)

xl_writer.save()
```

## openpyxl

La librería `openpyxl` da mayor flexibilidad y permite manipular los archivos Excel.

Tenemos que tener en cuenta la jerarquía de los archivos de Excel.

* Workbook
    * Worksheet
        * Range
            * Cell

La documentación se encuentra en [https://openpyxl.readthedocs.io](https://openpyxl.readthedocs.io) y el repositorio en [https://bitbucket.org/openpyxl/openpyxl](https://bitbucket.org/openpyxl/openpyxl)

### load_workbook

La función `load_workbook` genera un objeto de tipo `Workbook` importando un archivo de Excel.

```python
import openpyxl
wb = openpyxl.load_workbook(os.path.join('user','treemap_separado.xlsx'))
```

### Worksheet

Las sheets de un workbook se acceden mediante la propiedad `worksheets` del mismo.

```python
wb.worksheets
```

Puedo seleccionar una sheet tanto por índice ya que es una lista, como por nombre de la sheet.

```python
ws1 = wb.worksheets[0]
ws2 = wb['No Global']
```

### delete_cols

El método `delete_cols` elimina columnas de un worksheet.

Toma como argumentos la columna (1-indexed) y la cantidad de columnas.

```python
ws1.delete_cols(2)
```

### save

La función `save` de un workbook guarda el archivo.

Esto no implica que el elemento `Workbook` desaparezca.

```python
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

### insert_rows

El método ``insert_rows` agrega filas antes de una fila en particular.

```python
ws2.insert_rows(1)
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

### Modificando valores

Seleccionando una celda como diccionario y asignando un valor se modifica el valor de la misma.

```python
ws2['A1']='Ubicación'
ws2['C1']='Valores'
```

También se pueden agregar fórmulas, teniendo en cuenta que las mismas tienen que estar en inglés y sus argumentos separados por coma.

```python
ws2["D530"]="=SUM(D3:D529)"
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

### merge_cells

El método `merge_cells` combina celdas de un rango.

Se puede escribir un string con un rango de Excel o bien con los argumentos `start_row`,`end_row`,`start_column`,`end_column`

```python
ws2.merge_cells('A1:B1')
ws2.merge_cells(start_row=1,end_row=1,start_column=3,end_column=4)
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

El método opuesto es `unmerge_cells`.

### styles

El módulo `styles` contiene los elementos que dan formato a las celdas.

Los valores por defecto en Excel son:

```python
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font

font = Font(name='Calibri',
                size=11,
                bold=False,
                italic=False,
                vertAlign=None,
                underline='none',
                strike=False,
                color='FF000000')

fill = PatternFill(fill_type=None,
                start_color='FFFFFFFF',
                end_color='FF000000')

border = Border(left=Side(border_style=None,
                          color='FF000000'),
                right=Side(border_style=None,
                           color='FF000000'),
                top=Side(border_style=None,
                         color='FF000000'),
                bottom=Side(border_style=None,
                            color='FF000000'),
                diagonal=Side(border_style=None,
                              color='FF000000'),
                diagonal_direction=0,
                outline=Side(border_style=None,
                             color='FF000000'),
                vertical=Side(border_style=None,
                              color='FF000000'),
                horizontal=Side(border_style=None,
                               color='FF000000')
               )

alignment=Alignment(horizontal='general',
                    vertical='bottom',
                    text_rotation=0,
                    wrap_text=False,
                    shrink_to_fit=False,
                    indent=0)

number_format = 'General'

protection = Protection(locked=True,
                        hidden=False)
```

Para aplicar un style a una celda, se tiene que seleccionar la celda y la propiedad y cambiar por un style de los descriptos arriba.

```python
ws2['A3'].font=Font(bold=True)
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

#### Varios valores

Los formatos sólo pueden ser aplicados a una celda por vez, por lo tanto hay que iterar.

Al seleccionar un rango, genero una tupla de tuplas, conteniendo a nivel superior filas y luego columnas.


```python
for fila in ws2['A3:B529']:
    for celda in fila:
        celda.font=Font(bold=True)
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

### NamedStyle

Un objeto de tipo `NamedStyle` contiene varios estilos (como el cerdo de Homero).

Después de crearlo, se le irán agregando propiedades.

```python
from openpyxl.styles import NamedStyle
ciclon = NamedStyle('Ciclón')
ciclon.font = Font(size=14,name='Tahoma',bold=True, color='0000FF')
ciclon.fill = PatternFill(fill_type="solid",fgColor='FF0000')
```

Luego, hay que asignarlo al workbook.

```python
wb.add_named_style(ciclon)
```

Finalmente, se le asigna a las celdas ese estilo.

```python
ws2['A1'].style = 'Ciclón'
ws2['C1'].style = 'Ciclón'
wb.save(os.path.join('user','treemap con formato.xlsx'))
```

>En el caso de seleccionar celdas mergeadas, tomar como referencia el extremo izquierdo superior.
