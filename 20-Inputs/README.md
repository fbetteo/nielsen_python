# Inputs

Existen diversas formas de pasarle parámetros a un script en el momento de de ejecutarlo.

## input

La función `input` va a imprimir en consola un mensaje y va a esperar a que nosotros escribamos algo y apretemos `Enter`.

Aquello que nosotros escribamos va a ingresar al sistema como `str` y se va a comportar de la misma forma que si lo escribiéramos. Es decir, si es lo último del script lo va a imprimir, sino, se pierde en caso de no almacenarlo en una variable o imprimirlo explícitamente.

```python
nombre = input('Cómo te llamás?\n')

estado = input('Hola {}, cómo estás?\n'.format(nombre))

if estado.lower()=='bien':
    print('Qué bueno!!!')
elif estado.lower()=='mal':
    print('Qué bajón!')
else:
    print("No podés responder como una persona normal???")
```

## sys.argv

El objeto `sys.argv` que contiene argumentos de la ejecución del script por línea de comandos.

Va a ser una lista y en el primer elemento va a estar el nombre del archivo. Luego, van a estar los siguientes argumentos que pasamos.

Para utilizarlo, tenemos que importar el módulo sys.

```python
import sys

for i,argumento in enumerate(sys.argv[1:]):
    print('{}:{}'.format(i,argumento))
```

## Archivos .ini

Los archivos `.ini` son archivos con el siguiente formato:

```ini
[sección 1]
variable=valor
variable=valor

[seccion 2]
variable=valor
variable=valor
```

Para importarlos se utiliza un objeto de tipo `ConfigParser`.

```python
import configparser
config = configparser.ConfigParser()
```

>`ConfigParser` es un constructor. Es decir, un método que crea objetos de una clase particular.

Para abrir un archivo, se utiliza el método `read` que lleva como argumento la ruta del archivo.

```python
config.read('clase.ini')
```

Ahora el objeto va a tener un método `sections` que nos va a devolver una lista con las secciones del archivo.

```python
print('Las Secciones son:')
print(config.sections())
print('**************************')
```

Luego, esas secciones se pueden usar "como diccionarios".

```python
for s in config.sections():
    print('Sección: {}'.format(s))
    for k in config[s]:
        print('{}: {}'.format(k,config[s][k]))
    print('**************************')
```

## Archivos .json

Los archivos `.json` son un formato de data semiestructurada de representación clave-valor.

```json
{"cliente":"P&G",
"productos":["Shampoo","Acondicionador"],
"modelo":"PnP",
"moneda":"$",
"costo":500000,
"se corre en India":false}
```

Para importar el mismo se debe utilizar la librería `json`, importar el contenido del archivo como texto y utilizar la función `json.loads`.

```python
import json

with open('pnp.json') as f:
    datos = json.loads(f.read())

print(datos)
```
