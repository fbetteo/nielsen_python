import configparser
config = configparser.ConfigParser()

config.read('clase.ini')

print('Las Secciones son:')
print(config.sections())
print('**************************')

for s in config.sections():
    print('Sección: {}'.format(s))
    for k in config[s]:
        print('{}: {}'.format(k,config[s][k]))
    print('**************************')