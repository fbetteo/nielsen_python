# Condicionales

## Booleans

Los booleans son tipos de datos que pueden tomar sólo 2 valores `True` o `False`.

### Operadores Booleanos

* **Menor a:** `<`
* **Menor o igual a:** `<=`
* **Mayor a:** `>`
* **Mayor o igual a:** `>=`
* **Igual a:** `==`
* **Desigual a:** `!=`

### or

El operador `or` va a devolver `True` cuando alguna de las 2 condiciones sea True.

```python
(3>4) or (3%2==0)
```

### and

El operador and va a devolver `True` sólo si ambas condiciones son True.

```python
(3>4) and (3%2==0)
```

### not

El operador `not` previo a un boolean devuelve lo opesto.

```python
not 3>4
```

## Devolver un valor

Para devolver un valor valor si se cumple una condición, la sintaxis es `<valor si se cumple la condición> if <condición> else <valor si no se cumple al condición>`

```python
numero = 3
par_o_impar = 'PAR' if numero%2==0 else 'IMPAR'
print('{} es {}'.format(numero,par_o_impar))
```

## Estructura de control

### if

Para cambiar el flujo de ejecución del programa de acuerdo a una condición se usa el `if` statement con la sintaxis `if <condición>:`.

Como Python es un lenguaje identado, lo que se ejecutará de cumplirse la condición irá con tabulación debajo del if.

```python
numero = 3
if numero%2==0:
    print('{} es PAR'.format(numero))
print('Fin de la Ejecución')
```

### else

Para ejecutar una porción de código cuando no se cumple la condición se debe poner la sentencia `else:` al mismo nivel de indentación que el if.

```python
numero = 3
if numero%2==0:
    print('{} es PAR'.format(numero))
else:
    print('{} es IMPAR'.format(numero))
print('Fin de la Ejecución')
```

### elif

Si quiero ejecutar si no se cumple una condición pero se debe cumplir otra, tengo que utilizar la sentencia `elif <condición>:`

```python
numero=3
if numero>10:
    print('{} es mayor a 10'.format(numero))
elif numero>7:
    print('{} es mayor a 7'.format(numero))
elif numero>2:
    print('{} es mayor a 2'.format(numero))
else:
    print('{} es menor o igual a 2'.format(numero))
print('Fin de la Ejecución')
```

### Condicionales anidados

Dentro del código a ejecutar se puede incorporar otro bloque condicional.

```python
numero=5
if numero%2!=0:
    if not numero<=5:
        print('{} es impar y mayor que 5'.format(numero))
    else:
        print('{} es impar y menor o igual a 5'.format(numero))
else:
    print('{} es par'.format(numero))
print('Fin de la Ejecución')
```
